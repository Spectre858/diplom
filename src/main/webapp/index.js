/**
 * Created by Andrey on 22.02.2017.
 */

var responseMap = [];
var _request = {};
var requestMessageCount = 0;
var responseMessageCount = 0;
var historyMessageCount = 0;
var activeTab = 1;
var currentUser = "Unknown";
var currentPage = 1;
var countPage;
var pages = [];
var rawAtPage = 8;


$(document).ready(function () {
    currentUser = getCookie('USER');
    console.log("ready " + currentUser);

    $("#user").html(currentUser);

    $("#main_form").submit(function (event) {
        event.preventDefault();
        mySub();
    });

    $("#logout").click(function (event) {
        event.preventDefault();
        logout();
    });

    loadHistory();
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace('/([\.$?*|{}\(\)\[\]\\\/\+^])/g', '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


function changePageLabel() {
    var label = document.getElementById("slider_label");
    var tn = document.createTextNode(currentPage + " / " + countPage);
    label.replaceChild(tn, label.firstChild);
}

function pageUp() {
    changePage(parseInt(currentPage) + 1);
}
function pageDown() {
    changePage(parseInt(currentPage) - 1);
}

function splitAtPages(documents) {
    var pages = [];
    var page = [];

    for (var iter = 0, i = 0; iter < documents.length; ++iter, ++i) {
        if (i == rawAtPage) {
            pages.push(page);
            page = [];
            i = 0;
        }
        page.push(documents[iter]);
    }
    if (i != 0) {
        pages.push(page);
    }
    return pages;
}

function changeFabVisibility() {
    var down = document.getElementById("fab_down_page");
    var up = document.getElementById("fab_up_page");
    if (currentPage == countPage && currentPage == 1) {
        down.style.visibility = "hidden";
        up.style.visibility = "hidden";
        return;
    }
    down.style.visibility = "visible";
    up.style.visibility = "visible";
    switch (currentPage) {
        case 1:
            down.style.visibility = "hidden";
            break;
        case countPage:
            up.style.visibility = "hidden";
            break;
    }
}

function changePage(value) {
    if (!countPage) {
        return;
    }
    document.getElementById("slider").value = value;
    currentPage = parseInt(value);
    changeFabVisibility();
    changePageLabel();
    drawHistPage(pages[currentPage - 1]);
}

function logout() {
    $.ajax({
        type: "GET",
        url: "/rest/auth/logout",
        contentType: false,
        processData: false,
        cache: false,
        timeout: 600000,
        success: function (dat) {
            console.log(dat);
            currentUser = "Unknown";
            $(location).attr("href", "/");
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
}

function clearTable() {
    var container = document.getElementById("tab-3-table");
    while (container.firstElementChild) {
        container.removeChild(container.firstElementChild);
    }
}

function drawHistPage(page) {
    clearTable();

    console.log(page);
    for (var i = 0; i < page.length; ++i) {
        console.log("-> " + i);
        var data = page[i];
        console.log(data);

        _request.first = data.order.agent.FIO.First + " " + data.order.agent.FIO.Surname;
        _request.last = data.order.applicant.name;
        addHistory(data.response.message, data.caseNumber, data.id, data.response.date);

        console.log(responseMap[i]);
    }
}
function loadHistory() {
    $.ajax({
        type: "GET",
        url: "/rest/order",
        contentType: false,
        processData: false,
        cache: false,
        timeout: 600000,
        success: function (dat) {
            console.log(dat);

            if (!dat) {
                return;
            }

            for (var i = 0; i < dat.length; ++i) {
                var id = generateRequestId();

                var node = {
                    id: id,
                    order: dat[i].order,
                    response: dat[i].response,
                    caseNumber: dat[i].caseNumber
                };

                responseMap.push(node);
            }

            pages = splitAtPages(responseMap);
            countPage = pages.length;

            changeFabVisibility();
            changePageLabel();

            var slider = document.getElementById("slider");
            slider.max = countPage;



            drawHistPage(pages[currentPage - 1]);

            historyMessageCount = dat.length;
            updateBadges();
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
}

function changeVisibility(icon, field) {
    if (field.is(':hidden')) {
        field.show();
        icon.html("expand_less");
    } else {
        field.hide();
        icon.html("expand_more");
    }
}

function fileUpload() {
    console.log("start file upload");
    var pb = $("#filePB");
    var icon = $("#filePB_icon");
    pb.addClass("is-active");

    var file = $("#file_upload")[0].files[0];

    console.log(file);

    var data = new FormData();
    data.append(file.name, file);

    console.log(data);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/rest/file/upload",
        data: data,
        contentType: false,
        processData: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("SUCCESS : ", data);
            $("#file_id").val(data.fid);
            $("#file_name").val(data.fileName);
            $("#file_path").val(data.path);
            pb.removeClass("is-active");
            icon.html("done");
            icon.removeClass("hidden");
        },
        error: function (e) {
            console.log("ERROR : ", e);
            pb.removeClass("is-active");
            icon.html("error");
            icon.removeClass("hidden");
        }
    });
}
function checkRequired() {
    console.log("check");
    var clear = true;
    var first = null;

    // $(".mdl-textfield.is-invalid .mdl-textfield__input").each(function () {
    //     clear = false;
    //     if (first == null) {
    //         first = $(this);
    //     }
    // });
    $("input[required]").each(function () {
        if ($(this).val() == "") {
            clear = false;
            if (first == null) {
                first = $(this);
            }
        }
    });
    if (!clear) {
        console.log("error: " + first);
        toast("Some required fields are empty or has wrong format " + first.name);
    }
    return clear;
}

function updateProperties() {
    console.log("invalid");
    $(".is-invalid").each(function () {
        console.log($(this));
        $(this).removeClass("is-invalid");
        $(this).addClass("is-dirty");
    })
}

function resetForm() {
    $("#filePB_icon").addClass("hidden");
    $("#main_form")[0].reset();
}

function showResponseTab() {
    $("#respPB").removeClass("hidden");
    $("#req").removeClass("is-active");
    $("#resp").addClass("is-active");

    $("#scroll-tab-1").removeClass("is-active");
    $("#scroll-tab-2").addClass("is-active");


}

function showRequestTab() {
    $("#hist").removeClass("is-active");
    $("#req").addClass("is-active");

    $("#scroll-tab-3").removeClass("is-active");
    $("#scroll-tab-1").addClass("is-active");


}

function mySub() {

    console.log("TEST");
    $("#btn_submit_status").addClass("hidden");
    if (!checkRequired()) {
        return;
    }
    order = getCurrentOrder();
    _request.first = $("#app_name").val();
    _request.last = $("#agent_surname").val();

    resetForm();

    showResponseTab();

    sendOrder(order);
    // sendUserAjax(_request, handleUser);
}

function sendOrder(order) {
    console.log(order);
    var requestId = generateRequestId();
    var node = {};
    node.id = requestId;
    node.order = order;

    $.ajax({
        type: "POST",
        url: "/rest/soap/createOrder",
        data: JSON.stringify(order),
        contentType: "application/json; charset=UTF-8",
        processData: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            responseMap.push(node);
            console.log("response -> " + data);
            $("#respPB").addClass("hidden");
            handleCreateOrderResponse(data, requestId);
        },
        error: function (e) {
            console.log("ERROR : ", e);
            var error = {
                type: e.status,
                message: e.statusText
            };
            node.response = error;
            responseMap.push(node);

            $("#respPB").addClass("hidden");
            ajaxError(error, requestId);
        }
    });
}

function handleCreateOrderResponse(response, requestId) {
    console.log("-----> RESPONSE: ");
    console.log(response);

    var error = {};

    if (response.error != null) {
        error.type = response.error.code;
        error.message = response.error.message;
    }

    for (var i = 0; i < responseMap.length; ++i) {
        if (responseMap[i].id == requestId) {

            responseMap[i].response = response.error == null ? response : error;
            responseMap[i].caseNumber = response.code;
            addResponse(responseMap[i].response);
            break;
        }
    }

    pages = splitAtPages(responseMap);

    countPage = pages.length;

    changeFabVisibility();
    changePageLabel();

    var slider = document.getElementById("slider");
    slider.max = countPage;

    drawHistPage(pages[currentPage - 1]);

    historyMessageCount++;
    responseMessageCount++;
    updateBadges();


    // if (response.error != null) {
    //     ajaxError(error, requestId);
    // } else {
    //     ajaxSuccess(response, requestId);
    // }
}

function ajaxSuccess(resp, requestId) {
    addResponse(resp);
    addHistory('success', resp.code, requestId);

    responseMessageCount = 1;
    historyMessageCount++;
    updateBadges();
}

function ajaxError(error, requestId) {

    addResponse(error);
    addHistory('error', error.message, requestId);

    responseMessageCount = 1;
    historyMessageCount++;
    updateBadges();
}

function addResponse(resp) {
    var mainCont = document.getElementById("response-card");

    var container = document.createElement("div");
    container.className = "center";

    var card = document.createElement("div");
    card.className = "demo-card-wide mdl-card mdl-shadow--2dp";

    var title = document.createElement("div");
    title.className = "mdl-card__title";
    var titHead = document.createElement("h2");
    titHead.className = "mdl-card__title-text";
    var titleText;

    var body = document.createElement("div");
    body.className = "mdl-card__supporting-text";
    var bodyText;

    var infoCont = document.createElement("div");
    infoCont.className = "center";

    var info = document.createElement("label");
    info.className = "mdl-textfield__label";
    info.className = "not_absolute";

    var data = document.createTextNode(resp.date);
    info.appendChild(data);



    //resp.date + " " + resp.message + " " +
    if (resp.code) {
        title.style.background = "url('img/success.jpg') center / cover";
        titleText = document.createTextNode("Запрос успешно обработан");
        bodyText = document.createTextNode(resp.code + " - ");
        body.appendChild(bodyText);

        if (resp.type) {
            data = document.createTextNode("; " + resp.type);
            info.appendChild(data);
        }
    } else {
        title.style.background = "url('img/error.jpg') center / cover";
        titleText = document.createTextNode("Ошибка при обработке запроса ");

        bodyText = document.createTextNode(resp.type);
        body.appendChild(bodyText);

    }
    if (resp.message) {
        bodyText = document.createTextNode(resp.message);
        body.appendChild(bodyText);
    }

    titHead.appendChild(titleText);
    title.appendChild(titHead);

    card.appendChild(title);
    card.appendChild(body);

    infoCont.appendChild(info);

    container.appendChild(infoCont);
    container.appendChild(card);

    if (mainCont.firstElementChild != null) {
        mainCont.replaceChild(container, mainCont.firstElementChild);
    } else {
        mainCont.appendChild(container);
    }
}

function addHistory(type, res, rid, date) {
    var container = document.getElementById("tab-3-table");

    var tr = document.createElement("tr");
    tr.onclick = function () {
        showRequest(this);
        // updateStatus(this);
    };

    var td1 = document.createElement("td");
    td1.className = "mdl-data-table__cell--non-numeric";
    var td2 = document.createElement("td");
    td2.className = "mdl-data-table__cell--non-numeric";
    var td3 = document.createElement("td");
    td3.className = "mdl-data-table__cell--non-numeric";
    var td4 = document.createElement("td");
    td4.className = "mdl-data-table__cell--non-numeric";
    var td5 = document.createElement("td");
    td5.className = "mdl-data-table__cell--non-numeric";
    var td6 = document.createElement("td");
    td6.id = "requestId";
    td6.className = "mdl-data-table__cell--non-numeric";
    td6.style.display = "none";

    var dateString = date;
    if (!date) {
        date = new Date();
        dateString = date.getDate() + "." + date.getMonth() + "." + date.getFullYear() + " "
            + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    }
    var dateField = document
        .createTextNode(dateString);
    var login = document
        .createTextNode(_request.first);
    var password = document
        .createTextNode(_request.last);
    var status = document
        .createTextNode(type);
    var result = document
        .createTextNode(res);
    var requestId = document
        .createTextNode(rid);

    td1.appendChild(dateField);
    td2.appendChild(login);
    td3.appendChild(password);
    td4.appendChild(status);
    td5.appendChild(result);
    td6.appendChild(requestId);

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    container.appendChild(tr);

    addRowToMap(tr, rid);
}

function updateStatus(id) {
    console.log("ID: " + id);
    resetForm();
    $("#btn_submit_status").addClass("hidden");
    var row = null;
    for (var i = 0; i < responseMap.length; ++i) {
        if (responseMap[i].id == id) {
            row = responseMap[i].row;
            break;
        }
    }
    if (row == null) {
        console.log("row is null");
        return;
    }
    console.log("update status");
    var date = row.childNodes.item(0);
    var status = row.childNodes.item(3);
    var result = row.childNodes.item(4);

    if (status.innerHTML == 'error') {
        return;
    }
    var url = "/rest/soap/getStatus/" + result.innerHTML;
    console.log("url: " + url);

    $.ajax({
        type: "GET",
        url: url,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            status.innerHTML = data.code + ": " + data.message;
            date.innerHTML = data.date;
            addResponse(data);
            historyMessageCount = 1;
            responseMessageCount = 1;
            updateBadges();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            addResponse(e);
            responseMessageCount = 1;
            updateBadges();
        }
    });

}

function addRowToMap(row, id) {
    console.log(id);
    for (var i = 0; i < responseMap.length; ++i) {
        if (responseMap[i].id == id) {
            responseMap[i].row = row;
            break;
        }
    }
}
function showRequest(row) {
    id = row.lastElementChild.innerHTML;


    console.log(id);
    for (var i = 0; i < responseMap.length; ++i) {
        console.log(responseMap[i]);
        if (responseMap[i].id == id) {
            console.log("equals");
            addResponse(responseMap[i].response);
            console.log("response added");
            fillRequest(responseMap[i].order);

            $("#requestId").val(id);
            console.log("request added");

            requestMessageCount = 1;
            responseMessageCount = 1;
            updateBadges();
            $("#btn_submit_status").removeClass("hidden");
            break;
        }
    }
    // updateProperties();

}

function toast(text) {
    var snackbarContainer = document.querySelector('#demo-toast-example');
    var data = {message: text};
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function generateRequestId() {
    return Math.random().toString(36).substr(3, 12);
}

function clickTab(tab) {
    switch (tab) {
        case 'req':
            activeTab = 1;
            break;
        case 'resp':
            activeTab = 2;
            break;
        case 'hist':
            activeTab = 3;
            break;
    }
    clearBadges(tab);
}

function clearBadges(tab) {
    if (tab == 'resp') {
        responseMessageCount = 0;
    } else if (tab == 'hist') {
        historyMessageCount = 0;
    } else {
        requestMessageCount = 0;
    }
    updateBadges();
}

function updateBadges() {
    var req = document.getElementById("badges_request");
    var resp = document.getElementById("badges_response");
    var hist = document.getElementById("badges_history");


    if (activeTab == 1) {
        requestMessageCount = 0;
    }
    if (activeTab == 2) {
        responseMessageCount = 0;
    }
    if (activeTab == 3) {
        historyMessageCount = 0;
    }

    if (requestMessageCount == 0) {
        req.style.visibility = "hidden";
    } else {
        req.setAttribute("data-badge", requestMessageCount);
        req.style.visibility = "visible";
    }

    if (responseMessageCount == 0) {
        resp.style.visibility = "hidden";
    } else {
        resp.setAttribute("data-badge", responseMessageCount);
        resp.style.visibility = "visible";
    }

    if (historyMessageCount == 0) {
        hist.style.visibility = "hidden";
    } else {
        hist.setAttribute("data-badge", historyMessageCount);
        hist.style.visibility = "visible";
    }

}

function fillRequest(order) {
    $("#declarant_kind").val(order.declarantKind).change();

    $("#app_name").val(order.applicant.name);
    $("#app_code").val(order.applicant.code).change();
    $("#app_email").val(order.applicant.email);

    $("#app_postal_code").val(order.applicant.location.Postal_Code);
    $("#app_region").val(order.applicant.location.Region);
    $("#app_city").val(order.applicant.location.City.Name);
    $("#app_street").val(order.applicant.location.Street.Name);
    $("#app_level1").val(order.applicant.location.Level1.Value);
    $("#app_apartment").val(order.applicant.location.Apartment.Value);
    $("#app_other").val(order.applicant.location.Other);
    $("#app_note").val(order.applicant.location.Note);

    $("#agent_surname").val(order.agent.FIO.Surname);
    $("#agent_firstname").val(order.agent.FIO.First);
    $("#agent_patronymic").val(order.agent.FIO.Patronymic);

    $("#agent_document_code").val(order.agent.Document.Code_Document).change();
    $("#agent_document_series").val(order.agent.Document.Series);
    $("#agent_document_number").val(order.agent.Document.Number);
    $("#agent_document_date").val(order.agent.Document.Date);
    $("#agent_document_organ").val(order.agent.Document.IssueOrgan);

    $("#agent_postal_code").val(order.agent.Location.Postal_Code);
    $("#agent_region").val(order.agent.Location.Region);
    $("#agent_city").val(order.agent.Location.City.Name);
    $("#agent_street").val(order.agent.Location.Street.Name);
    $("#agent_level1").val(order.agent.Location.Level1.Value);
    $("#agent_apartment").val(order.agent.Location.Apartment.Value);
    $("#agent_other").val(order.agent.Location.Other);
    $("#agent_note").val(order.agent.Location.Note);

    $("#agent_email").val(order.agent['E-mail']);
    $("#agent_phone").val(order.agent.Phone);
    $("#agent_snils").val(order.agent.SNILS);
    $("#agent_kind").val(order.agent.agent_kind).change();

    $("#object_postal_code").val(order.objectLocation.Postal_Code);
    $("#object_region").val(order.objectLocation.Region);
    $("#object_city").val(order.objectLocation.City.Name);
    $("#object_street").val(order.objectLocation.Street.Name);
    $("#object_level1").val(order.objectLocation.Level1.Value);
    $("#object_apartment").val(order.objectLocation.Apartment.Value);
    $("#object_other").val(order.objectLocation.Other);
    $("#object_note").val(order.objectLocation.Note);

    $("#response_email").val(order.responseEmail);

    $("#document_code").val(order.appliedDocument.code).change();
    $("#document_number").val(order.appliedDocument.number);
    $("#document_date").val(order.appliedDocument.date);

    $("#file_id").val(order.appliedDocument.file.fid);
    $("#file_name").val(order.appliedDocument.file.fileName);
    $("#file_path").val(order.appliedDocument.file.path);

    updateProperties();

}

function getCurrentOrder() {
    return {
        "declarantKind": $("#declarant_kind :selected").val(),
        "applicant": {
            "name": $("#app_name").val(),
            "code": $("#app_code :selected").val(),
            "email": $("#app_email").val(),
            "location": {
                "Postal_Code": $("#app_postal_code").val(),
                "Region": $("#app_region").val(),
                "City": {
                    "Name": $("#app_city").val(),
                    "Type": "г"
                },
                "Street": {
                    "Name": $("#app_street").val(),
                    "Type": "ул"
                },
                "Level1": {
                    "Type": "д",
                    "Value": $("#app_level1").val()
                },
                "Level2": null,
                "Level3": null,
                "Apartment": {
                    "Type": "кв",
                    "Value": $("#app_apartment").val()
                },
                "Other": $("#app_other").val(),
                "Note": $("#app_note").val()
            }
        },
        "agent": {
            "FIO": {
                "Surname": $("#agent_surname").val(),
                "First": $("#agent_firstname").val(),
                "Patronymic": $("#agent_patronymic").val()
            },
            "Document": {
                "Code_Document": $("#agent_document_code :selected").val(),
                "Series": $("#agent_document_series").val(),
                "Number": $("#agent_document_number").val(),
                "Date": $("#agent_document_date").val(),
                "IssueOrgan": $("#agent_document_organ").val()
            },
            "Location": {
                "Postal_Code": $("#agent_postal_code").val(),
                "Region": $("#agent_region").val(),
                "City": {
                    "Name": $("#agent_city").val(),
                    "Type": "г"
                },
                "Street": {
                    "Name": $("#agent_street").val(),
                    "Type": "ул"
                },
                "Level1": {
                    "Type": "д",
                    "Value": $("#agent_level1").val()
                },
                "Level2": null,
                "Level3": null,
                "Apartment": {
                    "Type": "кв",
                    "Value": $("#agent_apartment").val()
                },
                "Other": $("#agent_other").val(),
                "Note": $("#agent_note").val()
            },
            "E-mail": $("#agent_email").val(),
            "Phone": $("#agent_phone").val(),
            "SNILS": $("#agent_snils").val(),
            "agent_kind": $("#agent_kind :selected").val()
        },
        "objectLocation": {
            "Postal_Code": $("#object_postal_code").val(),
            "Region": $("#object_region").val(),
            "City": {
                "Name": $("#object_city").val(),
                "Type": "г"
            },
            "Street": {
                "Name": $("#object_street").val(),
                "Type": "ул"
            },
            "Level1": {
                "Type": "д",
                "Value": $("#object_level1").val()
            },
            "Level2": null,
            "Level3": null,
            "Apartment": {
                "Type": "кв",
                "Value": $("#object_apartment").val()
            },
            "Other": $("#object_other").val(),
            "Note": $("#object_note").val()
        },
        "responseEmail": $("#response_email").val(),
        "appliedDocument": {
            "name": $("#document_code :selected").text(),
            "code": $("#document_code :selected").val(),
            "number": $("#document_number").val(),
            "date": $("#document_date").val(),
            "file": {
                "fid": $("#file_id").val(),
                "fileName": $("#file_name").val(),
                "path": $("#file_path").val()
            }
        }
    };
}