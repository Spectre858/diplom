

$(document).ready(function () {
    console.log("ready");

    $("#main_form").submit(function (event) {
        event.preventDefault();
        $("#waitPB").removeClass("hidden");

        var request = {
            login: $("#login").val(),
            password: $("#password").val()
        };
        console.log(request);


        $.ajax({
            type: "POST",
            url: "/rest/auth/login",
            data: JSON.stringify(request),
            contentType: "application/json; charset=UTF-8",
            processData: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                console.log("success: " + data);
                document.cookie = "USER=" + request.login;
                $("#waitPB").addClass("hidden");
                $(location).attr("href", "/");
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#waitPB").addClass("hidden");
                toast("Something go wring: [" + e.status + "] " + e.responseText);
            }
        });
    });

    $("#registration").click(function () {
        $(location).attr("href", "/registration");
    });
    console.log("invalid");

    $("input").each(function () {
        $(this).trigger('propertychange');
    });
});

function toast(text) {
    var snackbarContainer = document.querySelector('#demo-toast-example');
    var data = {message: text};
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}