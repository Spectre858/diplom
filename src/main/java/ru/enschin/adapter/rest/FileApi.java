package ru.enschin.adapter.rest;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import ru.enschin.adapter.util.AuthUtil;
import ru.enschin.adapter.services.FileService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;

/**
 * Created by Andrey on 08.05.2017.
 */
@ApplicationScoped
@Path("file")
public class FileApi {
    @Inject
    private Logger logger;
    @Inject
    private FileService fileService;


    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object uploadFile(MultipartFormDataInput input, @Context HttpServletRequest request) {
        String fileName;
        MultivaluedMap<String, String> header;
        for (InputPart inputPart : input.getParts()) {
            try {
                header = inputPart.getHeaders();
                fileName = getFileNameFromHeader(header);
                if (fileName != null) {
                    String contentType = header.getFirst("Content-Type");
                    String body = inputPart.getBodyAsString();
                    return fileService.storeFile(fileName, body);
                }
            } catch (IOException e) {
                logger.error("Error occured related to uploading file", e);
                throw new InternalServerErrorException("Failed to upload file");
            }
        }
        throw new BadRequestException("File not found in request");
    }

    private String getFileNameFromHeader(MultivaluedMap<String, String> header) {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
        for (String part : contentDisposition) {
            if ((part.trim().startsWith("filename"))) {
                String[] name = part.split("=");
                return name[1].trim().replaceAll("\"", "");
            }
        }
        return null;
    }
}
