package ru.enschin.adapter.rest;

import ru.enschin.adapter.dao.DaoService;
import ru.enschin.adapter.dao.OrientDaoService;
import ru.enschin.adapter.manager.producer.ResourceProperty;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.order.Applicant;
import ru.enschin.adapter.model.order.AppliedDocument;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.requestinfo.Agent;
import ru.enschin.adapter.model.soap.requestinfo.Fio;
import ru.enschin.adapter.model.soap.requestinfo.document.Document;
import ru.enschin.adapter.model.soap.requestinfo.location.City;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;
import ru.enschin.adapter.model.soap.requestinfo.location.LocationLevel;
import ru.enschin.adapter.model.soap.requestinfo.location.Street;
import ru.enschin.adapter.manager.OrientDBManager;
import ru.enschin.adapter.util.AuthUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Andrey on 29.04.2017.
 */
@ApplicationScoped
@Path("/")
public class HelloWorldController {

    @Inject
    @ResourceProperty("rest.folder")
    private String restFolder;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("hello")
    public Order hello(@Context HttpServletRequest request) {
        if (!AuthUtil.checkAuth(request)) {
            throw new NotAuthorizedException(Response.status(401).build());
        }
        City city = new City();
        city.setName("Новосибирск");
        city.setType("г");

        Street street = new Street();
        street.setName("Беловежская");
        street.setType("ул");

        LocationLevel level = new LocationLevel();
        level.setType("д");
        level.setValue("4/1");

        LocationLevel kv = new LocationLevel();
        kv.setType("кв");
        kv.setValue("250");

        Location location = new Location();
        location.setRegion("54");
        location.setCity(city);
        location.setStreet(street);
        location.setLevel1(level);
        location.setApartment(kv);

        Applicant applicant = new Applicant();
        applicant.setName("Пенсионный фонд россии");
        applicant.setCode("1234");
        applicant.setEmail("test@test.com");
        applicant.setLocation(location);

        Fio fio = new Fio();
        fio.setFirst("Andrey");
        fio.setSurname("Enshin");

        Document document = new Document();
        document.setSeries("1234");
        document.setNumber("123456");
        document.setDate("15.04.2013");
        document.setIssueOrgan("УФМС");

        Agent agent = new Agent();
        agent.setFio(fio);
        agent.setDocument(document);
        agent.setLocation(location);
        agent.setEmail("andrey@gmail.com");
        agent.setSnils("1235-1235");
        agent.setPhone("999-132-1123");

        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName("Заявление");
        appliedDocument.setFile(new StoredFile("1", "order.pdf", null));

        Order order = new Order();
        order.setApplicant(applicant);
        order.setAgent(agent);
        order.setObjectLocation(location);
        order.setAppliedDocument(appliedDocument);
        order.setResponseEmail("spectre@mail.ru");

        User user = new User();
        user.setLogin("test");
        user.setPassword("1234");
        user.setEmail("test@test");

        return order;
    }

}
