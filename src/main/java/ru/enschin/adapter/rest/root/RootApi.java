package ru.enschin.adapter.rest.root;

import com.sun.imageio.plugins.common.ImageUtil;
import org.apache.log4j.Logger;
import ru.enschin.adapter.services.FileService;
import ru.enschin.adapter.util.AuthUtil;
import ru.enschin.adapter.util.ContentType;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
@Path("/")
public class RootApi {
    @Inject
    private Logger logger;
    @Inject
    private FileService fileService;



    @GET
    @Path("/img/{image}")
    public Response getImage(@PathParam("image") String imageName) {
        logger.info("ROOT: " + imageName);

        Response.ResponseBuilder response = Response.ok();
        response.header("Content-Type", ContentType.IMAGE_JPEG);

        final File webFile = fileService.getWebFile("img" + File.separator + imageName);
        if (webFile == null) {
            return Response.status(404).build();
        }


        response.entity(new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                Files.copy(Paths.get(webFile.getAbsolutePath()), outputStream);

            }
        });

        return response.build();
    }

    @GET
    @Path("/{element : [0-9a-zA-Z_]+.(css|js)}")
    @Produces({ ContentType.CSS_UTF_8, ContentType.JS_UTF_8 })
    public Object getRootCssElement(@PathParam("element") String element) throws IOException {
        logger.info("ROOT: " + element);

        return fileService.getWebFile(element);
    }
//    @GET
//    @Path("/{element : [0-9a-zA-Z_]+.js}")
//    @Produces(JS_UTF_8)
//    public Object getRootJsElement(@PathParam("element") String element) throws IOException {
//        logger.info("ROOT: " + element);
//
//        return fileService.getWebFile(element);
//    }

    @GET
    @Path("/login")
    @Produces(ContentType.HTML_UTF_8)
    public Object login() throws IOException {
        logger.info("login");

        return fileService.getWebFile("login.html");
    }

    @GET
    @Path("/registration")
    @Produces(ContentType.HTML_UTF_8)
    public Object registration() throws IOException {
        logger.info("registration");

        return fileService.getWebFile("registration.html");
    }

    @GET
    @Path("/")
    @Produces(ContentType.HTML_UTF_8)
    public Object getRootElement(@Context HttpServletRequest request) {
        if (AuthUtil.checkAuth(request)) {
            return fileService.getWebFile("index.html");
        }
        return fileService.getWebFile("login.html");
    }
}
