package ru.enschin.adapter.rest.root;

import org.apache.log4j.Logger;
import ru.enschin.adapter.util.AuthUtil;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Andrey on 05.06.2017.
 */
@Provider
public class AuthFilter implements ContainerRequestFilter {
    @Context
    private HttpServletRequest request;
    @Context
    private HttpServletResponse response;

    @Inject
    private Logger logger;

    private static final String LOGIN_PAGE = "/login";
    private static final String REGISTRATION_PAGE = "/registration";
    private static final String ROOT_PAGE = "/";
    private static final String LOGIN_REST = "/auth/";
    private static final String IMAGE_REST = "/img/";


    @Override
    public void filter(ContainerRequestContext containerRequest) throws WebApplicationException, IOException {
        String path = containerRequest.getUriInfo().getPath();
        logger.info("request to " + path);
        if (LOGIN_PAGE.equals(path) || ROOT_PAGE.equals(path)
                || REGISTRATION_PAGE.equals(path)
                || path.endsWith(".css") || path.endsWith(".js")
                || path.startsWith(LOGIN_REST) || path.startsWith(IMAGE_REST)) {
            return;
        }

        if (AuthUtil.checkAuth(request)) {
            return;
        }

        logger.info("denied");
        containerRequest.abortWith(Response.status(401).build());
    }
}