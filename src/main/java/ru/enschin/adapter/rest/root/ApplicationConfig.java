package ru.enschin.adapter.rest.root;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
@ApplicationPath("/")
public class ApplicationConfig extends Application {

    private final Set<Class<?>> classes;


    public ApplicationConfig() {
        HashSet<Class<?>> c = new HashSet<Class<?>>();
        c.add(AuthFilter.class);
        c.add(RootApi.class);
        this.classes = Collections.unmodifiableSet(c);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}