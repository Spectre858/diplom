package ru.enschin.adapter.rest;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import ru.enschin.adapter.dao.DaoService;
import ru.enschin.adapter.manager.AuthManager;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.util.AuthUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
@Path("auth")
public class AuthApi {
    @Inject
    private AuthManager authManager;
    @Inject
    private Logger logger;

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Object login(User user, @Context HttpServletRequest request) {
        logger.info("login: " + user);
        if (!authManager.login(request, user)) {
            return Response.status(400).build();
        }
        return Response.status(200).build();
    }

    @GET
    @Path("logout")
    @Consumes(MediaType.APPLICATION_JSON)
    public Object logout(@Context HttpServletRequest request) {
        logger.info("logout");

        AuthUtil.removeCurrentUser(request);

        return Response.status(200).build();
    }

    @POST
    @Path("create/user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Object createUser(User user, @Context HttpServletRequest request) {
        logger.info("create user: " + user);
        if (!authManager.create(request, user)) {
            return Response.status(403).entity("Такой пользователь уже существует").build();
        }
        return Response.status(200).build();
    }
}
