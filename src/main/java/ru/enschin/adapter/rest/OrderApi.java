package ru.enschin.adapter.rest;

import ru.enschin.adapter.dao.DaoService;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.dao.StoredOrder;
import ru.enschin.adapter.util.AuthUtil;
import ru.enschin.adapter.util.ContentType;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.List;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
@Path("order")
public class OrderApi {
    @Inject
    private DaoService daoService;

    @GET
    @Path("/")
    @Produces(ContentType.APPLICATION_JSON_UTF_8)
    public Object getAll(@Context HttpServletRequest request) {
        User user = AuthUtil.currentUser(request);

        List<StoredOrder> storedOrders = daoService.selectAllOrdersByUser(user.getObjectId());

        if (storedOrders != null && !storedOrders.isEmpty()) {
            return storedOrders;
        } else {
            return null;
        }
    }
}
