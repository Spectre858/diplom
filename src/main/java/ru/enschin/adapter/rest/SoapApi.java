package ru.enschin.adapter.rest;

import com.alibaba.fastjson.JSON;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import ru.enschin.adapter.dao.DaoService;
import ru.enschin.adapter.model.Error;
import ru.enschin.adapter.model.RestResponse;
import ru.enschin.adapter.model.dao.StoredOrder;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.body.*;
import ru.enschin.adapter.model.soap.header.HeaderFactory;
import ru.enschin.adapter.model.soap.requestinfo.RequestGKN;
import ru.enschin.adapter.model.soap.requestinfo.RequestInfoFactory;
import ru.enschin.adapter.services.*;
import ru.enschin.adapter.util.AuthUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Andrey on 06.05.2017.
 */
@ApplicationScoped
@Path("soap")
public class SoapApi {
    @Inject
    private Logger logger;

    @Inject
    private SOAPService soapService;
    @Inject
    private XmlService xmlService;
    @Inject
    private CryptoService cryptoService;
    @Inject
    private CompressionService compressionService;
    @Inject
    private FileService fileService;
    @Inject
    private DaoService daoService;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    private static final String REQ_FILENAME = "req_%s.xml";
    private static final String REQ_SIGN_FILENAME = "req_%s.xml.sig";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("createOrder")
    public Object createOrder(Order order, @Context HttpServletRequest request) {
        RestResponse response = new RestResponse();
        response.setType("Error");

        Error error = new Error();
        error.setCode("Server error");
        response.setError(error);

        StoredOrder storedOrder = new StoredOrder();
        storedOrder.setOrder(order);
        storedOrder.setApplicant(AuthUtil.currentUser(request));
        storedOrder.setResponse(response);

        boolean withAppliedFile = !order.getAppliedDocument().getFile().getFid().isEmpty();

        String uuid = UUID.randomUUID().toString();

        RequestInfoFactory requestInfoFactory = new RequestInfoFactory();
        requestInfoFactory.setOrder(order);
        requestInfoFactory.setUuid(uuid);

        RequestGKN requestGKN = requestInfoFactory.build();
        String xml;
        try {
            xml = xmlService.objectToXml(requestGKN, RequestGKN.class);
            xml = cryptoService.toCanonicalize(xml.getBytes("UTF-8"));

            logger.info("Request: \n" + xml);

            List<String> name = new ArrayList<>();
            name.add(String.format(REQ_FILENAME, uuid));
            name.add(String.format(REQ_SIGN_FILENAME, uuid));
            if (withAppliedFile) {
                name.add(uuid + order.getAppliedDocument().getFile().getFileName());
            }

            List<String> value = new ArrayList<>();
            value.add(xml);
            value.add(cryptoService.getFileSign(xml.getBytes("UTF-8")));
            if (withAppliedFile) {
                value.add(fileService.readFile(order.getAppliedDocument().getFile().getPath()));
            }

            byte[] zip = compressionService.toZip(name, value, "UTF-8");

            fileService.storeFile("test.zip", new String(zip));
            Files.write(
                    Paths.get("test.zip"), zip,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            xml = Base64.encodeBase64String(zip);
        } catch (Exception e) {
            logger.error("Some error: " + e.getMessage(), e);
            error.setMessage(e.getMessage());
            response.setError(error);
            daoService.save(storedOrder);
            return response;
        }

        HeaderFactory headerFactory = new HeaderFactory();

        AppDocument appDocument = new AppDocument();
        appDocument.setBinaryData(xml);
        appDocument.setRequestCode(String.format(REQ_FILENAME, uuid));
        AppDataCreateRequest appDataCreateRequest = new AppDataCreateRequest();

        BodyFactory bodyFactory = new BodyFactory()
                .setDate(sdf.format(new Date()))
                .setAppDocument(appDocument)
                .setAppDataCreateRequest(appDataCreateRequest)
                .setOriginator(new ru.enschin.adapter.model.soap.body.User("FMS001001", "ФМС"))
                .setRecipient(new ru.enschin.adapter.model.soap.body.User("RRTR01001", "Росреестр"))
                .setSender(new ru.enschin.adapter.model.soap.body.User("FMS001001", "ФМС"));

        try {
            String soap = soapService.buildCreateOrderSoapRequest(headerFactory, bodyFactory);
            logger.info("Full request: \n" + soap);
            response = soapService.sendMessage(soap);
            response.setMessage("Request created");
            storedOrder.setResponse(response);
            storedOrder.setCaseNumber(response.getCode());
            logger.info("RESPONSE: " + response);
        } catch (Exception e) {
            logger.error("Some error: " + e.getMessage(), e);
            error.setMessage(e.getMessage());
            daoService.save(storedOrder);
            return response;
        }

        daoService.save(storedOrder);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getStatus/{orderId}")
    public RestResponse getStatus(@PathParam("orderId") String orderId) {
        RestResponse response = new RestResponse();
        response.setType("Error");

        StoredOrder storedOrder = new StoredOrder();
        storedOrder.setCaseNumber(orderId);
        storedOrder.setResponse(response);

        Error error = new Error();
        error.setCode("Server error");
        response.setError(error);

        HeaderFactory headerFactory = new HeaderFactory();
        AppDataGetStatus appDataGetStatus = new AppDataGetStatus(orderId);

        BodyFactory bodyFactory = new BodyFactory()
                .setDate(sdf.format(new Date()))
                .setAppDataGetStatus(appDataGetStatus)
                .setOriginator(new ru.enschin.adapter.model.soap.body.User("FMS001001", "ФМС"))
                .setRecipient(new ru.enschin.adapter.model.soap.body.User("RRTR01001", "Росреестр"))
                .setSender(new ru.enschin.adapter.model.soap.body.User("FMS001001", "ФМС"));
        try {
            String soap = soapService.buildGetStatusSoapRequest(headerFactory, bodyFactory);
            response = soapService.sendMessage(soap);
            storedOrder.setResponse(response);
        } catch (Exception e) {
            logger.error("Some error: " + e.getMessage(), e);
            error.setMessage(e.getMessage());
            daoService.updateOrder(storedOrder);
            return response;
        }

        daoService.updateOrder(storedOrder);
        return response;
    }

}
