package ru.enschin.adapter.rest;

import ru.enschin.adapter.rest.root.AuthFilter;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrey on 29.04.2017.
 */
@ApplicationScoped
@ApplicationPath("/rest")
public class RestConfig extends Application {

    private final Set<Class<?>> classes;


    public RestConfig() {
        HashSet<Class<?>> c = new HashSet<Class<?>>();
        c.add(HelloWorldController.class);
        c.add(SoapApi.class);
        c.add(FileApi.class);
        c.add(AuthApi.class);
        c.add(AuthFilter.class);
        c.add(OrderApi.class);
        this.classes = Collections.unmodifiableSet(c);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}
