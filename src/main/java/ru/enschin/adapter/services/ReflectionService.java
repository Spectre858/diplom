package ru.enschin.adapter.services;

import org.apache.log4j.Logger;
import ru.enschin.adapter.manager.FileManager;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey on 07.06.2017.
 */
@ApplicationScoped
public class ReflectionService {
    @Inject
    private Logger logger;
    @Inject
    private FileManager fileManager;



    public List<Class> getStoredEntityClasses() {
        List<Class> classes = new ArrayList<>();

        try {
            List<File> allFileWithFilter = fileManager.getAllFileWithFilter(".class");

            for (File file : allFileWithFilter) {
                String name = file.getPath()
                        .split(FileManager.FILE_SEPARATOR + "classes" + FileManager.FILE_SEPARATOR)[1]
                        .split("\\.")[0]
                        .replaceAll(FileManager.FILE_SEPARATOR, "\\.");
                Class<?> aClass;
                try {
                    aClass = Class.forName(name);
                } catch (Exception e) {
                    aClass = Object.class;
                }
                Class<?> iter = aClass;

                while (iter != null && !iter.isAssignableFrom(Object.class)) {
                    if (iter.isAssignableFrom(StoredEntity.class)) {
                        classes.add(aClass);
                    }
                    iter = iter.getSuperclass();
                }
            }
        } catch (Exception e) {
            logger.error("Some error while get stored entities files", e);
            return null;
        }
        return classes;
    }
}
