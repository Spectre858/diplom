package ru.enschin.adapter.services;

import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import ru.enschin.adapter.manager.producer.ResourceProperty;
import ru.enschin.adapter.model.Error;
import ru.enschin.adapter.model.RestResponse;
import ru.enschin.adapter.model.soap.Envelope;
import ru.enschin.adapter.model.soap.body.Body;
import ru.enschin.adapter.model.soap.body.BodyFactory;
import ru.enschin.adapter.model.soap.header.Header;
import ru.enschin.adapter.model.soap.header.HeaderFactory;
import ru.enschin.adapter.model.soap.header.SignedInfo;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.soap.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by Andrey on 06.05.2017.
 */
@ApplicationScoped
public class SOAPService {
    @Inject
    private CryptoService cryptoService;
    @Inject
    private XmlService xmlService;

    @Inject
    private Logger logger;

    @Inject
    @ResourceProperty("soap.destination.url")
    private String destinationURL;

    private SOAPConnectionFactory soapConnFactory;

    @PostConstruct
    public void init() {
        try {
            soapConnFactory = SOAPConnectionFactory.newInstance();
        } catch (SOAPException e) {
            logger.fatal("Can't create new instance of SOAPConnectionFactory", e);
        }
    }

    public RestResponse sendMessage(String soapXml) throws SOAPException, IOException {
        SOAPConnection connection = null;
        RestResponse response = new RestResponse();

        try {
            connection = soapConnFactory.createConnection();

            ByteArrayInputStream mes = new ByteArrayInputStream(soapXml.getBytes("UTF-8"));

            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage message = messageFactory
                    .createMessage(new MimeHeaders(), mes);

            SOAPMessage resp = connection.call(message, destinationURL);

            SOAPBody body = resp.getSOAPPart().getEnvelope().getBody();

            Node node = body.getFirstChild();
            Node info = node.getFirstChild();
            Node data = node.getLastChild();

            String date = info.getChildNodes().item(5).getTextContent();//date
            response.setDate(date);

            if (body.getFault() != null) {
                Error error = new Error();
                error.setCode(body.getFault().getFaultCode());
                error.setMessage(body.getFault().getFaultString());
                response.setError(error);
                response.setType("Error");
                return response;
            }
            if (node.getLocalName().equals("getStatusResponse")) {
                Node nd = data.getFirstChild().getFirstChild();
                response.setCode(nd.getFirstChild().getTextContent());//code
                response.setMessage(nd.getLastChild().getTextContent());//message
                response.setType(node.getLocalName());
            } else {
                response.setCode(data.getFirstChild().getTextContent());//code
                response.setType(node.getLocalName());
            }
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

        return response;
    }

    public String buildCreateOrderSoapRequest(HeaderFactory headerFactory, BodyFactory bodyFactory) throws Exception {
        return buildSoapRequest(headerFactory, bodyFactory.buildCreateRequest());
    }

    public String buildGetStatusSoapRequest(HeaderFactory headerFactory, BodyFactory bodyFactory) throws Exception {
        return buildSoapRequest(headerFactory, bodyFactory.buildGetStatusRequest());
    }


    private String buildSoapRequest(HeaderFactory headerFactory, Body body) throws Exception {
        String bodyHash = cryptoService.getHash(xmlService.objectToXml(body, Body.class).getBytes("UTF-8"));
        headerFactory.setDigestValue(bodyHash);

        SignedInfo signedInfo = headerFactory.buildSignedInfo();
        String signedInfoSignature = cryptoService
                .getSignature(xmlService.objectToXml(signedInfo, SignedInfo.class).getBytes("UTF-8"));
        headerFactory.setSignatureValue(signedInfoSignature)
                .setTokenValue(cryptoService.getCertification());

        Header header = headerFactory.buildHeader();

        Envelope envelope = new Envelope(header, body);

        return cryptoService.toCanonicalize(xmlService.objectToXml(envelope, Envelope.class).getBytes("UTF-8"));
    }

}
