package ru.enschin.adapter.services;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.xml.sax.SAXException;
import ru.enschin.adapter.manager.producer.ResourceProperty;
import ru.infotecs.crypto.ViPNetProvider;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * Created by Andrey on 01.05.2017.
 */
@ApplicationScoped
public class CryptoService {
    @Inject
    @ResourceProperty("crypto.sign.algorithm")
    private String signAlgorithm;
    @Inject
    @ResourceProperty("crypto.sign.provider")
    private String signProvider;
    @Inject
    @ResourceProperty("crypto.key.container")
    private String keyContainer;
    @Inject
    @ResourceProperty("crypto.sign.version")
    private String signVersion;

    @Inject
    private FileService fileService;
    @Inject
    private Logger logger;

    private KeyStore keyStore;
    private String alias;
    private char[] password;
    private MessageDigest digestDriver;


    public String getHash(byte[] xml) throws IOException, ParserConfigurationException, SAXException, CanonicalizationException, InvalidCanonicalizerException {
        String canonXML = toCanonicalize(xml);
        digestDriver.update(canonXML.getBytes("UTF-8"));
        byte[] digestValue = digestDriver.digest();
        String hash = DatatypeConverter.printBase64Binary(digestValue);
        return hash;
    }

    public String getSignature(byte[] xml) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, CertificateEncodingException, NoSuchProviderException, InvalidKeyException, SignatureException, IOException, ParserConfigurationException, SAXException, CanonicalizationException, InvalidCanonicalizerException {
        Signature signatureDriver = Signature.getInstance(signAlgorithm, signProvider);
        PrivateKey pKey = (PrivateKey) keyStore.getKey(alias, password);
        signatureDriver.initSign(pKey);
        String canonXML = toCanonicalize(xml);
        byte[] nextDataChunk = canonXML.getBytes("UTF-8");
        signatureDriver.update(nextDataChunk);
        byte[] signatureValue = signatureDriver.sign();
        String sign = Base64.encodeBase64String(signatureValue);
        return sign;
    }

    public String getFileSign(byte[] data) throws NoSuchProviderException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, InvalidKeyException, SignatureException {
        Signature signatureDriver = Signature.getInstance(signAlgorithm, signProvider);
        PrivateKey pKey = (PrivateKey) keyStore.getKey(alias, password);
        signatureDriver.initSign(pKey);
        signatureDriver.update(data);
        byte[] signatureValue = signatureDriver.sign();
        String sign = Base64.encodeBase64String(signatureValue);
        return sign;
    }

    public String getCertification() throws CertificateEncodingException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        X509Certificate cert = (X509Certificate) keyStore.getCertificate(alias);
        String certificate = Base64.encodeBase64String(cert.getEncoded());
        return certificate;
    }

    public String toCanonicalize(byte[] xml) throws IOException, ParserConfigurationException, SAXException, CanonicalizationException, InvalidCanonicalizerException {
        Canonicalizer canon = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
        byte[] canonBody = canon.canonicalize(xml);
        String canonXML = new String(canonBody, "UTF-8");
        return canonXML;
    }

    @PostConstruct
    public void initBeginParams() {
        org.apache.xml.security.Init.init();
        Security.addProvider(new ViPNetProvider());
        try {
            digestDriver = MessageDigest.getInstance(signVersion, signProvider);
            keyStore = KeyStore.getInstance(keyContainer, signProvider);
            keyStore.load(new FileInputStream(fileService.getSign()), null);
        } catch (Exception e) {
            logger.fatal("Can't initialize the crypto service, cause: " + e, e);
            return;
        }
        alias = "key";
        password = "123456".toCharArray();

        logger.info("inited: " + org.apache.xml.security.Init.isInitialized());
    }
}
