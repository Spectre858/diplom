package ru.enschin.adapter.services;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.*;

/**
 * Created by Andrey on 06.05.2017.
 */
public class CompressionService {
    @Inject
    private Logger logger;

    public byte[] toZip(List<String> files, List<String> data, String encoding) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(outStream, Charset.forName(encoding));

        if (files.size() != data.size()) {
            return null;
        }

        for (int i = 0; i < files.size(); ++i) {
            String file = files.get(i);
            byte[] content = data.get(i).getBytes(encoding);

            try {
                ZipEntry zipEntry = new ZipEntry(file);
                out.putNextEntry(zipEntry);
                out.write(content);
            } finally {
                out.closeEntry();

            }
        }
        out.close();

        return outStream.toByteArray();
    }

    public void unZip(List<String> fileNames, List<String> content, String data, String encoding) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes(encoding));
        ZipInputStream in = new ZipInputStream(inputStream);

        ZipEntry entry = null;
        byte[] buf = new byte[1024 * 10];
        int countByte = 0;

        while ((entry = in.getNextEntry()) != null) {
            fileNames.add(entry.getName());

            while ((countByte = in.read(buf)) > 0) {
                outStream.write(buf, 0, countByte);
            }
            in.closeEntry();
            content.add(new String(outStream.toByteArray(), encoding));
            outStream.reset();
        }
        in.close();

        outStream.toByteArray();
    }



    public byte[] toZip(String text, String encoding) throws IOException {
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();

        Deflater deflater = new Deflater();
        deflater.setLevel(Deflater.BEST_COMPRESSION);
        deflater.setInput(text.getBytes(encoding));
        deflater.finish();

        byte[] buf = new byte[2048];
        int countByte = 0;

        while (!deflater.finished()) {
            countByte = deflater.deflate(buf);
            if (countByte != 0) {
                byteOutputStream.write(buf, 0, countByte);
            }
        }
        byteOutputStream.flush();

        byte[] result = byteOutputStream.toByteArray();
        byteOutputStream.close();

        return result;
    }

    public byte[] unZip(byte[] zip) throws DataFormatException, IOException {
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();

        Inflater inflater = new Inflater();
        inflater.setInput(zip);

        byte[] buf = new byte[2048];
        int countByte = 0;

        while (!inflater.finished()) {
            countByte = inflater.inflate(buf);
            if (countByte != 0) {
                byteOutputStream.write(buf, 0, countByte);
            }
        }
        byteOutputStream.flush();

        byte[] result = byteOutputStream.toByteArray();
        byteOutputStream.close();

        return result;
    }
}
