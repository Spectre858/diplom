package ru.enschin.adapter.services;

import ru.enschin.adapter.model.soap.Envelope;
import ru.enschin.adapter.model.soap.NSPMapper;
import ru.enschin.adapter.model.soap.body.Body;
import ru.enschin.adapter.model.soap.body.BodyFactory;
import ru.enschin.adapter.model.soap.header.Header;
import ru.enschin.adapter.model.soap.header.HeaderFactory;
import ru.enschin.adapter.model.soap.header.SignedInfo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

/**
 * Created by Andrey on 01.05.2017.
 */
@ApplicationScoped
public class XmlService {

    public String objectToXml(Object object, Class cl) throws JAXBException {
        StringWriter result = new StringWriter();
        Marshaller marshaller = JAXBContext.newInstance(cl)
                .createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPMapper());
        marshaller.marshal(object, result);
        return result.toString();
    }

}
