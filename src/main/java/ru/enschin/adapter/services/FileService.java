package ru.enschin.adapter.services;

import org.apache.log4j.Logger;
import ru.enschin.adapter.manager.FileManager;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Andrey on 07.05.2017.
 */
@ApplicationScoped
public class FileService {
    @Inject
    private FileManager fileManager;
    @Inject
    private Logger logger;

    private static final String SIGN_FILE_NAME = "sign/rnd-4-83ab-849f-bf72-9154-3174-cbe6-8037";
    private static final String FILE_STORAGE = "files";

    public List<File> getAllPropertyFiles() {
        List<File> result = new ArrayList<>();
        try {
            result = fileManager.getAllFileWithFilter(".properties");
        } catch (Exception e) {
            logger.error("Some error while get All Properties Files: ", e);
            return result;
        }
        return result;
    }

    public File getSign() {
        File file = null;
        try {
            file = fileManager.getResource(SIGN_FILE_NAME);
        } catch (Exception e) {
            logger.error("Can't get file sign", e);
            return null;
        }
        return file;
    }

    public StoredFile storeFile(String name, String data) {
        String uuid = UUID.randomUUID().toString();
        String path = FILE_STORAGE + "/" + uuid;
        try {

            File file = fileManager.getFile(path);
            if (file == null) {
                file = fileManager.createFile(path);
                if (file == null) {
                    logger.error("Can't create file " + path);
                    return null;
                }
            }

            Files.write(Paths.get(file.getAbsolutePath()), data.getBytes("UTF-8"),
                    StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
            logger.info("Stored at " + file.getAbsolutePath());
            return new StoredFile(uuid, name, path);
        } catch (Exception e) {
            logger.error("Can't write file " + path + " cause: ", e);
            return null;
        }
    }

    public String readFile(String fileName) {
        try {
            File file = fileManager.getFile(fileName);
            byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            return new String(bytes);
        } catch (Exception e) {
            logger.error("Can't get file " + fileName, e);
            return null;
        }
    }

    public String readResource(String resourceName) {
        try {
            File file = fileManager.getResource(resourceName);
            byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            return new String(bytes);
        } catch (Exception e) {
            logger.error("Can't get file " + resourceName, e);
            return null;
        }
    }

    public String readWebFile(String fileName) {
        try {
            File file = fileManager.getWebFile(fileName);
            byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            return new String(bytes);
        } catch (Exception e) {
            logger.error("Can't get file " + fileName, e);
            return null;
        }
    }

    public File getWebFile(String fileName) {
        try {
            return fileManager.getWebFile(fileName);
        } catch (Exception e) {
            return null;
        }
    }
}
