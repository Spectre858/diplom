package ru.enschin.adapter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.codehaus.jackson.annotate.JsonIgnore;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import java.io.Serializable;

/**
 * Created by Andrey on 29.04.2017.
 */
public class User extends StoredEntity {
    @JsonProperty
    private String login;
    @JsonProperty
    private String password;
    @JsonProperty
    private String email;

    public User() {
    }

    public User(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
