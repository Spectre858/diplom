package ru.enschin.adapter.model.dao;

import ru.enschin.adapter.model.RestResponse;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.order.Order;

/**
 * Created by Andrey on 05.06.2017.
 */
public class StoredOrder extends StoredEntity {
    private Order order;
    private String caseNumber;
    private User applicant;
    private RestResponse response;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public User getApplicant() {
        return applicant;
    }

    public void setApplicant(User applicant) {
        this.applicant = applicant;
    }

    public RestResponse getResponse() {
        return response;
    }

    public void setResponse(RestResponse response) {
        this.response = response;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }
}
