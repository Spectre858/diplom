package ru.enschin.adapter.model.dao;

import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.util.UUID;

/**
 * Created by Andrey on 05.06.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class StoredEntity {
    @Id
    @XmlTransient
    private String id;
    @Version
    @XmlTransient
    private String version;
    @XmlTransient
    private String objectId;

    public StoredEntity() {
        objectId = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
