package ru.enschin.adapter.model;

import ru.enschin.adapter.model.dao.StoredEntity;

/**
 * Created by Andrey on 08.05.2017.
 */
public class StoredFile extends StoredEntity {
    private String fid;
    private String fileName;
    private String path;

    public StoredFile() {
    }

    public StoredFile(String fid, String fileName, String path) {
        this.fid = fid;
        this.fileName = fileName;
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
