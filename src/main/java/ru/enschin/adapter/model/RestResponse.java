package ru.enschin.adapter.model;

import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;

/**
 * Created by Andrey on 06.05.2017.
 */
public class RestResponse extends StoredEntity {
    @JsonProperty
    private String date;
    @JsonProperty
    private String code;
    @JsonProperty
    private String message;
    @JsonProperty
    private String type;
    @JsonProperty
    private Error error;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
