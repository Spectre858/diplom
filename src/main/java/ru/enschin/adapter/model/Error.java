package ru.enschin.adapter.model;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Andrey on 06.05.2017.
 */
public class Error {
    @JsonProperty
    private String code;
    @JsonProperty
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
