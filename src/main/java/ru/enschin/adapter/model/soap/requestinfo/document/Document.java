package ru.enschin.adapter.model.soap.requestinfo.document;

import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Document extends StoredEntity {
    @XmlElement(name = "Code_Document")
    @JsonProperty
    private String code = "008001001000";

    @XmlElement(name = "Series")
    @JsonProperty
    private String series;

    @XmlElement(name = "Number")
    @JsonProperty
    private String number;

    @XmlElement(name = "Date")
    @JsonProperty
    private String date;

    @XmlElement(name = "IssueOrgan")
    @JsonProperty
    private String issueOrgan;

    public Document() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIssueOrgan() {
        return issueOrgan;
    }

    public void setIssueOrgan(String issueOrgan) {
        this.issueOrgan = issueOrgan;
    }
}
