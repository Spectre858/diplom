package ru.enschin.adapter.model.soap;

import ru.enschin.adapter.model.soap.body.Body;
import ru.enschin.adapter.model.soap.header.Header;

import javax.xml.bind.annotation.*;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlRootElement(name = "Envelope", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Envelope {
    @XmlElementRef
    private Header header;
    @XmlElementRef
    private Body body;

    public Envelope() {
    }

    public Envelope(Header header, Body body) {
        this.header = header;
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
