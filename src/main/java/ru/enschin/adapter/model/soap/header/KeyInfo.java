package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KeyInfo {
    @XmlElement(name = "SecurityTokenReference", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    private SecurityTokenReference securityTokenReference;

    public KeyInfo() {
        securityTokenReference = new SecurityTokenReference();
    }

    public KeyInfo(SecurityTokenReference securityTokenReference) {
        this.securityTokenReference = securityTokenReference;
    }

    public SecurityTokenReference getSecurityTokenReference() {
        return securityTokenReference;
    }

    public void setSecurityTokenReference(SecurityTokenReference securityTokenReference) {
        this.securityTokenReference = securityTokenReference;
    }
}
