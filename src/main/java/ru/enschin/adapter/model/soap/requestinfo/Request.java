package ru.enschin.adapter.model.soap.requestinfo;

import ru.enschin.adapter.model.soap.requestinfo.document.AppliedDocuments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
    @XmlElement(name = "Declarant")
    private Declarant declarant;

    @XmlElement(name = "RequiredData")
    private RequiredData requiredData;

    @XmlElement(name = "Delivery")
    private Delivery delivery;

    @XmlElement(name = "Applied_Documents")
    private AppliedDocuments appliedDocuments;

    @XmlElement(name = "MunicipalService")
    private MunicipalService municipalService;

    public Request() {
        municipalService = new MunicipalService();
        appliedDocuments = new AppliedDocuments();
        delivery = new Delivery();
        requiredData = new RequiredData();
    }

    public Declarant getDeclarant() {
        return declarant;
    }

    public void setDeclarant(Declarant declarant) {
        this.declarant = declarant;
    }

    public RequiredData getRequiredData() {
        return requiredData;
    }

    public void setRequiredData(RequiredData requiredData) {
        this.requiredData = requiredData;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public AppliedDocuments getAppliedDocuments() {
        return appliedDocuments;
    }

    public void setAppliedDocuments(AppliedDocuments appliedDocuments) {
        this.appliedDocuments = appliedDocuments;
    }

    public MunicipalService getMunicipalService() {
        return municipalService;
    }

    public void setMunicipalService(MunicipalService municipalService) {
        this.municipalService = municipalService;
    }
}
