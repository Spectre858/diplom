package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KsZuKs {
    @XmlElement(name = "ObjectLot")
    private ObjectLot objectLot;

    public KsZuKs() {
        objectLot = new ObjectLot();
    }

    public ObjectLot getObjectLot() {
        return objectLot;
    }

    public void setObjectLot(ObjectLot objectLot) {
        this.objectLot = objectLot;
    }
}
