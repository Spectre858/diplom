package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Reference {
    @XmlAttribute(name = "URI")
    private String uriAtr = "#body";

    @XmlElement(name = "Transforms", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private Transforms transforms;
    @XmlElement(name = "DigestMethod", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DigestMethod digestMethod;
    @XmlElement(name = "DigestValue", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private String digestValue;

    public Reference() {
        transforms = new Transforms();
        digestMethod = new DigestMethod();
    }

    public Reference(Transforms transforms, DigestMethod digestMethod, String digestValue) {
        this.transforms = transforms;
        this.digestMethod = digestMethod;
        this.digestValue = digestValue;
    }

    public void setUriAtr(String uriAtr) {
        this.uriAtr = uriAtr;
    }

    public String getUriAtr() {
        return uriAtr;
    }


    public Transforms getTransforms() {
        return transforms;
    }

    public void setTransforms(Transforms transforms) {
        this.transforms = transforms;
    }

    public DigestMethod getDigestMethod() {
        return digestMethod;
    }

    public void setDigestMethod(DigestMethod digestMethod) {
        this.digestMethod = digestMethod;
    }

    public String getDigestValue() {
        return digestValue;
    }

    public void setDigestValue(String digestValue) {
        this.digestValue = digestValue;
    }
}
