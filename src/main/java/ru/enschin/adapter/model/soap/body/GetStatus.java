package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlRootElement(name = "getStatusRequest", namespace = "http://portal.fccland.ru/rt/")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetStatus {
    @XmlElement(name = "Message")
    private Message message;
    @XmlElement(name = "MessageData")
    private MessageDataGetStatus messageDataGetStatus;

    public GetStatus() {
    }

    public GetStatus(Message message, MessageDataGetStatus messageDataGetStatus) {
        this.message = message;
        this.messageDataGetStatus = messageDataGetStatus;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public MessageDataGetStatus getMessageDataGetStatus() {
        return messageDataGetStatus;
    }

    public void setMessageDataGetStatus(MessageDataGetStatus messageDataGetStatus) {
        this.messageDataGetStatus = messageDataGetStatus;
    }
}
