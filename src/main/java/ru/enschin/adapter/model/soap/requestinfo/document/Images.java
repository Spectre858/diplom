package ru.enschin.adapter.model.soap.requestinfo.document;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Images {
    @XmlElement(name = "Image")
    private Image image;

    public Images() {
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
