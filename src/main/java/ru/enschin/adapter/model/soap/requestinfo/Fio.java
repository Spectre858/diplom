package ru.enschin.adapter.model.soap.requestinfo;

import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Fio extends StoredEntity {
    @XmlElement(name = "Surname")
    @JsonProperty
    private String surname;

    @XmlElement(name = "First")
    @JsonProperty
    private String first;

    @XmlElement(name = "Patronymic")
    @JsonProperty
    private String patronymic;

    public Fio() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}
