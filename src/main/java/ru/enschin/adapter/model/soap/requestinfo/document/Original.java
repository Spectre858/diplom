package ru.enschin.adapter.model.soap.requestinfo.document;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Original {
    @XmlAttribute(name = "Quantity")
    private String quantity = "1";
    @XmlAttribute(name = "Quantity_Sheet")
    private String quantitySheet = "1";

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantitySheet() {
        return quantitySheet;
    }

    public void setQuantitySheet(String quantitySheet) {
        this.quantitySheet = quantitySheet;
    }
}
