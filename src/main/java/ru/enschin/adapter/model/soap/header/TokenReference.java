package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TokenReference {
    @XmlAttribute(name = "URI")
    private String uriAtr = "#Cert";
    @XmlAttribute(name = "ValueType")
    private String valueTypeAtr = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3";

    public TokenReference() {
    }

    public TokenReference(String uriAtr, String valueTypeAtr) {
        this.uriAtr = uriAtr;
        this.valueTypeAtr = valueTypeAtr;
    }

    public String getUriAtr() {
        return uriAtr;
    }

    public void setUriAtr(String uriAtr) {
        this.uriAtr = uriAtr;
    }

    public String getValueTypeAtr() {
        return valueTypeAtr;
    }

    public void setValueTypeAtr(String valueTypeAtr) {
        this.valueTypeAtr = valueTypeAtr;
    }
}