package ru.enschin.adapter.model.soap.requestinfo.location;

import ru.enschin.adapter.model.dao.StoredEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class City extends StoredEntity{
    @XmlAttribute(name = "Name")
    private String name;
    @XmlAttribute(name = "Type")
    private String type;

    public City() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
