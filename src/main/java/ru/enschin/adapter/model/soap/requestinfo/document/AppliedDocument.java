package ru.enschin.adapter.model.soap.requestinfo.document;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AppliedDocument {
    @XmlElement(name = "Code_Document")
    private String code;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "Number")
    private String number;
    @XmlElement(name = "Date")
    private String date;
    @XmlElement(name = "Images")
    private Images images;
    @XmlElement(name = "Quantity")
    private Quantity quantity;

    public AppliedDocument() {
        quantity = new Quantity();
        images = new Images();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }
}
