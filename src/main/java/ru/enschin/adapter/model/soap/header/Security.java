package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Security {
    @XmlAttribute(name = "soap:actor")
    private String actorAtr = "http://smev.gosuslugi.ru/actors/smev";

    @XmlElement(name = "BinarySecurityToken", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    private BinarySecurityToken token;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private Signature signature;

    public Security() {
    }

    public Security(BinarySecurityToken token, Signature signature) {
        this.token = token;
        this.signature = signature;
    }

    public void setActorAtr(String actorAtr) {
        this.actorAtr = actorAtr;
    }

    public String getActorAtr() {
        return actorAtr;
    }

    public BinarySecurityToken getToken() {
        return token;
    }

    public void setToken(BinarySecurityToken token) {
        this.token = token;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }
}
