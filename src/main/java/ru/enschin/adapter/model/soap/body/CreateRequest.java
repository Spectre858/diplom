package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Andrey on 02.03.2017.
 */
@XmlRootElement(name = "createRequestRequest", namespace = "http://portal.fccland.ru/rt/")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateRequest {
    @XmlElement(name = "Message")
    private Message message;
    @XmlElement(name = "MessageData")
    private MessageDataCreateRequest messageDataCreateRequest;

    public CreateRequest() {
    }

    public CreateRequest(Message message, MessageDataCreateRequest messageDataCreateRequest) {
        this.message = message;
        this.messageDataCreateRequest = messageDataCreateRequest;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public MessageDataCreateRequest getMessageDataCreateRequest() {
        return messageDataCreateRequest;
    }

    public void setMessageDataCreateRequest(MessageDataCreateRequest messageDataCreateRequest) {
        this.messageDataCreateRequest = messageDataCreateRequest;
    }
}
