package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery {
    @XmlElement(name = "E_mailAddress")
    private String email;

    public Delivery() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
