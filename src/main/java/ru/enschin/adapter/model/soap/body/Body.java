package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.*;

/**
 * Created by Andrey on 02.03.2017.
 */
@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
//    xmlns="http://smev.gosuslugi.ru/rev111111"
    @XmlAttribute(name = "xmlns")
    private String xmlns = "http://smev.gosuslugi.ru/rev111111";
    @XmlAttribute(name = "wsu:Id")
    private String idAttribute = "body";

    @XmlElement(name = "createRequestRequest", namespace = "http://portal.fccland.ru/rt/")
    private CreateRequest createRequest;

    @XmlElement(name = "getStatusRequest", namespace = "http://portal.fccland.ru/rt/")
    private GetStatus getStatus;

    public Body() {
    }

    public Body(CreateRequest createRequest) {
        this.createRequest = createRequest;
    }

    public Body(GetStatus getStatus) {
        this.getStatus = getStatus;
    }

    public String getIdAttribute() {
        return idAttribute;
    }

    public void setIdAttribute(String idAttribute) {
        this.idAttribute = idAttribute;
    }

    public CreateRequest getCreateRequest() {
        return createRequest;
    }

    public void setCreateRequest(CreateRequest createRequest) {
        this.createRequest = createRequest;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public GetStatus getGetStatus() {
        return getStatus;
    }

    public void setGetStatus(GetStatus getStatus) {
        this.getStatus = getStatus;
    }
}
