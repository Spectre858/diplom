package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AppDataGetStatus {
    @XmlElement(name = "requestNumber", namespace = "http://portal.fccland.ru/types/")
    private String requestNumber;

    public AppDataGetStatus() {
    }

    public AppDataGetStatus(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }
}
