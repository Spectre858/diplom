package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AppDocument {
    @XmlElement(name = "RequestCode")
    private String requestCode = "req.xml";
    @XmlElement(name = "BinaryData")
    private String binaryData;

    public AppDocument() {
    }

    public AppDocument(String requestCode, String binaryData) {
        this.requestCode = requestCode;
        this.binaryData = binaryData;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(String binaryData) {
        this.binaryData = binaryData;
    }
}
