package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BinarySecurityToken {
    @XmlAttribute(name = "EncodingType")
    private String encodingAtr = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary";
    @XmlAttribute(name = "ValueType")
    private String valueTypeAtr = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3";
    @XmlAttribute(name = "wsu:Id")
    private String idAtr = "Cert";

    @XmlValue
    private String value;

    public BinarySecurityToken() {
    }

    public BinarySecurityToken(String idAtr, String value) {
        this.idAtr = idAtr;
        this.value = value;
    }

    public void setEncodingAtr(String encodingAtr) {
        this.encodingAtr = encodingAtr;
    }

    public void setValueTypeAtr(String valueTypeAtr) {
        this.valueTypeAtr = valueTypeAtr;
    }

    public String getEncodingAtr() {
        return encodingAtr;
    }

    public String getValueTypeAtr() {
        return valueTypeAtr;
    }

    public String getIdAtr() {
        return idAtr;
    }

    public void setIdAtr(String idAtr) {
        this.idAtr = idAtr;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
