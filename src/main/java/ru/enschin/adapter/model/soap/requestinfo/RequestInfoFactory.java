package ru.enschin.adapter.model.soap.requestinfo;

import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.requestinfo.document.AppliedDocument;
import ru.enschin.adapter.model.soap.requestinfo.document.Image;

/**
 * Created by Andrey on 04.05.2017.
 */
public class RequestInfoFactory {
    private Order order;
    private String uuid;

    private boolean withAppliedFile;

    public RequestInfoFactory() {
    }

    public RequestInfoFactory(Order order, String uuid) {
        this.order = order;
        this.uuid = uuid;
    }

    public RequestGKN build() {
        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName(order.getAppliedDocument().getName());
        appliedDocument.setCode(order.getAppliedDocument().getCode());
        appliedDocument.setDate(order.getAppliedDocument().getDate());
        appliedDocument.setNumber(order.getAppliedDocument().getNumber());
        if (withAppliedFile) {
            Image image = new Image();
            String name = order.getAppliedDocument().getFile().getFid()
                    + "_" + order.getAppliedDocument().getFile().getFileName();
            image.setName(name);
            appliedDocument.getImages().setImage(image);
        } else {
            appliedDocument.setImages(null);
        }

        Governance governance = new Governance();
        governance.setName(order.getApplicant().getName());
        governance.setCode(order.getApplicant().getCode());
        governance.setEmail(order.getApplicant().getEmail());
        governance.setLocation(order.getApplicant().getLocation());
        governance.setAgent(order.getAgent());

        Declarant declarant = new Declarant();
        declarant.setGovernance(governance);
        declarant.setDeclarantKind(String.valueOf(order.getDeclarantKind()));

        Request request = new Request();
        request.setDeclarant(declarant);
        request.getDelivery().setEmail(order.getResponseEmail());
        request.getRequiredData().getKsZuKs().getObjectLot().setLocation(order.getObjectLocation());
        request.getAppliedDocuments().setAppliedDocument(appliedDocument);

        RequestGKN result = new RequestGKN();
        result.geteDocument().setGuid(uuid);
        result.setRequest(request);

        return result;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
        withAppliedFile = !order.getAppliedDocument().getFile().getFid().isEmpty();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
