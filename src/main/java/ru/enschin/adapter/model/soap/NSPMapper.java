package ru.enschin.adapter.model.soap;


import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

/**
 * Created by Andrey on 04.03.2017.
 */
public class NSPMapper extends NamespacePrefixMapper {
//    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    private static final String SOAP_PREFIX = "soap";
    private static final String SOAP_URI = "http://schemas.xmlsoap.org/soap/envelope/";
//    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
    private static final String WSSE_PREFIX = "wsse";
    private static final String WSSE_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    //    xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    private static final String WSU_PREFIX = "wsu";
    private static final String WSU_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
//    xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
    private static final String DS_PREFIX = "ds";
    private static final String DS_URI = "http://www.w3.org/2000/09/xmldsig#";
//    xmlns:ns4="http://portal.fccland.ru/rt/"
    private static final String NS4_PREFIX = "ns4";
    private static final String NS4_URI = "http://portal.fccland.ru/rt/";
    // xmlns:ns3="http://www.w3.org/2004/08/xop/include"
    private static final String NS3_PREFIX = "ns3";
    private static final String NS3_URI = "http://www.w3.org/2004/08/xop/include";
// xmlns:ns2="http://portal.fccland.ru/types/"
    private static final String NS2_PREFIX = "ns2";
    private static final String NS2_URI = "http://portal.fccland.ru/types/";
//    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    private static final String XSI_PREFIX = "xsi";
    private static final String XSI_URI = "http://www.w3.org/2001/XMLSchema-instance";

    @Override
    public String getPreferredPrefix(String nsUri, String suggestion, boolean req) {
        switch (nsUri) {
            case SOAP_URI:
                return SOAP_PREFIX;
            case WSSE_URI:
                return WSSE_PREFIX;
            case DS_URI:
                return DS_PREFIX;
//            case XSI_URI:
//                return XSI_PREFIX;
            case WSU_URI:
                return WSU_PREFIX;
            case NS4_URI:
                return NS4_PREFIX;
            case NS3_URI:
                return NS3_PREFIX;
            case NS2_URI:
                return NS2_PREFIX;
            default:
                return suggestion;
        }
    }


    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] {
                SOAP_URI,
                WSSE_URI,
                DS_URI,
//                XSI_URI,
                WSU_URI,
                NS4_URI,
                NS3_URI,
                NS2_URI,
        };
    }
}
