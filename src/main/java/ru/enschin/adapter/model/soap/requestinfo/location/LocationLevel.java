package ru.enschin.adapter.model.soap.requestinfo.location;

import ru.enschin.adapter.model.dao.StoredEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LocationLevel extends StoredEntity {
    @XmlAttribute(name = "Type")
    private String type;
    @XmlAttribute(name = "Value")
    private String value;

    public LocationLevel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
