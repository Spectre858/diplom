package ru.enschin.adapter.model.soap.body;

/**
 * Created by Andrey on 02.03.2017.
 */
public class BodyFactory {
    private AppDocument appDocument;
    private AppDataCreateRequest appDataCreateRequest;
    private AppDataGetStatus appDataGetStatus;
    private User sender;
    private User recipient;
    private User originator;
    private String date;

    public Body buildCreateRequest() {
        Message message = new Message();
        message.setSender(sender);
        message.setRecipient(recipient);
        message.setOriginator(originator);
        message.setDate(date);

        MessageDataCreateRequest messageDataCreateRequest = new MessageDataCreateRequest();
        messageDataCreateRequest.setAppDocument(appDocument);
        messageDataCreateRequest.setAppDataCreateRequest(appDataCreateRequest);

        CreateRequest createRequest = new CreateRequest();
        createRequest.setMessage(message);
        createRequest.setMessageDataCreateRequest(messageDataCreateRequest);

        Body body = new Body();
        body.setCreateRequest(createRequest);
        return body;
    }

    public Body buildGetStatusRequest() {
        Message message = new Message();
        message.setSender(sender);
        message.setRecipient(recipient);
        message.setOriginator(originator);
        message.setDate(date);

        MessageDataGetStatus messageDataGetStatus = new MessageDataGetStatus();
        messageDataGetStatus.setAppDataGetStatus(appDataGetStatus);

        GetStatus getStatus = new GetStatus();
        getStatus.setMessage(message);
        getStatus.setMessageDataGetStatus(messageDataGetStatus);

        Body body = new Body();
        body.setGetStatus(getStatus);
        return body;
    }

    public BodyFactory setAppDocument(AppDocument appDocument) {
        this.appDocument = appDocument;
        return this;
    }

    public BodyFactory setSender(User sender) {
        this.sender = sender;
        return this;
    }

    public BodyFactory setRecipient(User recipient) {
        this.recipient = recipient;
        return this;
    }

    public BodyFactory setOriginator(User originator) {
        this.originator = originator;
        return this;
    }

    public BodyFactory setDate(String date) {
        this.date = date;
        return this;
    }

    public AppDataCreateRequest getAppDataCreateRequest() {
        return appDataCreateRequest;
    }

    public BodyFactory setAppDataCreateRequest(AppDataCreateRequest appDataCreateRequest) {
        this.appDataCreateRequest = appDataCreateRequest;
        return this;
    }

    public AppDataGetStatus getAppDataGetStatus() {
        return appDataGetStatus;
    }

    public BodyFactory setAppDataGetStatus(AppDataGetStatus appDataGetStatus) {
        this.appDataGetStatus = appDataGetStatus;
        return this;
    }
}
