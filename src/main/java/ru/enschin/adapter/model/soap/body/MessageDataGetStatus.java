package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageDataGetStatus {
    @XmlElement(name = "AppData")
    private AppDataGetStatus appDataGetStatus;

    public MessageDataGetStatus() {
    }

    public MessageDataGetStatus(AppDataGetStatus appDataGetStatus) {
        this.appDataGetStatus = appDataGetStatus;
    }

    public AppDataGetStatus getAppDataGetStatus() {
        return appDataGetStatus;
    }

    public void setAppDataGetStatus(AppDataGetStatus appDataGetStatus) {
        this.appDataGetStatus = appDataGetStatus;
    }
}
