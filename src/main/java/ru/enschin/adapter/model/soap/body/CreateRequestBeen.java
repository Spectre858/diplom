package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateRequestBeen {
    @XmlElement(name = "okato", namespace = "http://portal.fccland.ru/types/")
    private String okato = "29202808001";
    @XmlElement(name = "requestType", namespace = "http://portal.fccland.ru/types/")
    private String requestType = "558101010000";

    public CreateRequestBeen() {
    }

    public CreateRequestBeen(String okato, String requestType) {
        this.okato = okato;
        this.requestType = requestType;
    }

    public String getOkato() {
        return okato;
    }

    public void setOkato(String okato) {
        this.okato = okato;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
