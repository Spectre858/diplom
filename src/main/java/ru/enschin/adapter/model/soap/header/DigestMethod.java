package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DigestMethod {
    @XmlAttribute(name = "Algorithm")
    private String algorithmAtr = "http://www.w3.org/2001/04/xmldsig-more#gostr3411";

    public DigestMethod() {
    }

    public String getAlgorithmAtr() {
        return algorithmAtr;
    }

    public void setAlgorithmAtr(String algorithmAtr) {
        this.algorithmAtr = algorithmAtr;
    }
}
