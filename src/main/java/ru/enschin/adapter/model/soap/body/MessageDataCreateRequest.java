package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageDataCreateRequest {
    @XmlElement(name = "AppData")
    private AppDataCreateRequest appDataCreateRequest;
    @XmlElement(name = "AppDocument")
    private AppDocument appDocument;

    public MessageDataCreateRequest() {
    }

    public MessageDataCreateRequest(AppDataCreateRequest appDataCreateRequest, AppDocument appDocument) {
        this.appDataCreateRequest = appDataCreateRequest;
        this.appDocument = appDocument;
    }

    public AppDataCreateRequest getAppDataCreateRequest() {
        return appDataCreateRequest;
    }

    public void setAppDataCreateRequest(AppDataCreateRequest appDataCreateRequest) {
        this.appDataCreateRequest = appDataCreateRequest;
    }

    public AppDocument getAppDocument() {
        return appDocument;
    }

    public void setAppDocument(AppDocument appDocument) {
        this.appDocument = appDocument;
    }
}
