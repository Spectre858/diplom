package ru.enschin.adapter.model.soap.requestinfo;

import ru.enschin.adapter.model.soap.requestinfo.location.Location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Governance {
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "Governance_Code")
    private String code;
    @XmlElement(name = "E-mail")
    private String email;
    @XmlElement(name = "Location")
    private Location location;
    @XmlElement(name = "Agent")
    private Agent agent;

    public Governance() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
