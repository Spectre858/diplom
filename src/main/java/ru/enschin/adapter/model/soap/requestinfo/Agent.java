package ru.enschin.adapter.model.soap.requestinfo;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;
import ru.enschin.adapter.model.soap.requestinfo.document.Document;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(value = {"Email"}, ignoreUnknown = true)
public class Agent extends StoredEntity{
    @XmlElement(name = "FIO")
    @JsonProperty
    private Fio fio;

    @XmlElement(name = "Document")
    @JsonProperty
    private Document document;

    @XmlElement(name = "Location")
    @JsonProperty
    private Location location;

    @XmlElement(name = "E-mail")
    @JsonProperty()
    private String email;

    @XmlElement(name = "Phone")
    @JsonProperty
    private String phone;

    @XmlElement(name = "SNILS")
    @JsonProperty
    private String snils;

    @XmlElement(name = "agent_kind")
    @JsonProperty
    private String agentKind = "356003000000";

    public Agent() {
    }

    public Fio getFio() {
        return fio;
    }

    public void setFio(Fio fio) {
        this.fio = fio;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getAgentKind() {
        return agentKind;
    }

    public void setAgentKind(String agentKind) {
        this.agentKind = agentKind;
    }
}
