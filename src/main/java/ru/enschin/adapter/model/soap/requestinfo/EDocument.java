package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import java.util.UUID;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EDocument {
    @XmlAttribute(name = "Version")
    private String version = "1.03";

    @XmlAttribute(name = "GUID")
    private String guid;

    public EDocument() {
        guid = UUID.randomUUID().toString();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
