package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Declarant {
    @XmlAttribute(name = "declarant_kind")
    private String declarantKind = "357007000000";

    @XmlElement(name = "Governance")
    private Governance governance;

    public Declarant() {
    }

    public String getDeclarantKind() {
        return declarantKind;
    }

    public void setDeclarantKind(String declarantKind) {
        this.declarantKind = declarantKind;
    }

    public Governance getGovernance() {
        return governance;
    }

    public void setGovernance(Governance governance) {
        this.governance = governance;
    }
}
