package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.*;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RequestGKN")
public class RequestGKN {
    @XmlAttribute(name = "xmlns:xsi")
    private String xsi = "http://www.w3.org/2001/XMLSchema-instance";
    @XmlElement(name = "eDocument")
    private EDocument eDocument;

    @XmlElement(name = "Request")
    private Request request;

    public RequestGKN() {
        eDocument = new EDocument();
    }


    public EDocument geteDocument() {
        return eDocument;
    }

    public void seteDocument(EDocument eDocument) {
        this.eDocument = eDocument;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getXsi() {
        return xsi;
    }

    public void setXsi(String xsi) {
        this.xsi = xsi;
    }
}
