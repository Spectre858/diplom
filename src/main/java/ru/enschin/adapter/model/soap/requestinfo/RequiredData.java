package ru.enschin.adapter.model.soap.requestinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RequiredData {
    @XmlElement(name = "KS_ZU_KS")
    private KsZuKs ksZuKs;

    public RequiredData() {
        ksZuKs = new KsZuKs();
    }

    public KsZuKs getKsZuKs() {
        return ksZuKs;
    }

    public void setKsZuKs(KsZuKs ksZuKs) {
        this.ksZuKs = ksZuKs;
    }
}
