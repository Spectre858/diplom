package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 01.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AppDataCreateRequest {
    @XmlElement(name = "createRequestBean", namespace = "http://portal.fccland.ru/types/")
    private CreateRequestBeen createRequestBeen;

    public AppDataCreateRequest() {
        createRequestBeen = new CreateRequestBeen();
    }

    public AppDataCreateRequest(CreateRequestBeen createRequestBeen) {
        this.createRequestBeen = createRequestBeen;
    }

    public CreateRequestBeen getCreateRequestBeen() {
        return createRequestBeen;
    }

    public void setCreateRequestBeen(CreateRequestBeen createRequestBeen) {
        this.createRequestBeen = createRequestBeen;
    }
}
