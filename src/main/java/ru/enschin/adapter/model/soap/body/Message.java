package ru.enschin.adapter.model.soap.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Andrey on 02.03.2017.
 */
@XmlRootElement(name = "Message")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {
    @XmlElement(name = "Sender")
    private User sender;
    @XmlElement(name = "Recipient")
    private User recipient;
    @XmlElement(name = "Originator")
    private User originator;
    @XmlElement(name = "TypeCode")
    private String typeCode = "GSRV";
    @XmlElement(name = "Status")
    private String status = "REQUEST";
    @XmlElement(name = "Date")
    private String date;
    @XmlElement(name = "ExchangeType")
    private String exchangeType = "2";
    @XmlElement(name = "ServiceCode")
    private String serviceCode = "10000013628";
    @XmlElement(name = "CaseNumber")
    private String caseNumber = "1/1";
    @XmlElement(name = "TestMsg")
    private String testMessage = "TRUE";

    public Message() {
    }

    public Message(User sender, User recipient, User originator, String typeCode, String status,
                   String date, String exchangeType, String serviceCode, String caseNumber, String testMessage) {
        this.sender = sender;
        this.recipient = recipient;
        this.originator = originator;
        this.typeCode = typeCode;
        this.status = status;
        this.date = date;
        this.exchangeType = exchangeType;
        this.serviceCode = serviceCode;
        this.caseNumber = caseNumber;
        this.testMessage = testMessage;
    }

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public User getOriginator() {
        return originator;
    }

    public void setOriginator(User originator) {
        this.originator = originator;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }
}
