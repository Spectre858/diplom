package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityTokenReference {
    @XmlElement(name = "Reference", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    private TokenReference reference;

    public SecurityTokenReference() {
        reference = new TokenReference();
    }

    public SecurityTokenReference(TokenReference reference) {
        this.reference = reference;
    }

    public TokenReference getReference() {
        return reference;
    }

    public void setReference(TokenReference reference) {
        this.reference = reference;
    }
}
