package ru.enschin.adapter.model.soap.requestinfo.document;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AppliedDocuments {
    @XmlElement(name = "Applied_Document")
    private AppliedDocument appliedDocument;

    public AppliedDocuments() {
    }

    public AppliedDocument getAppliedDocument() {
        return appliedDocument;
    }

    public void setAppliedDocument(AppliedDocument appliedDocument) {
        this.appliedDocument = appliedDocument;
    }
}
