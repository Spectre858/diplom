package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SignatureMethod {
    @XmlAttribute(name = "Algorithm")
    private final String algorithmAtr = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";

    public SignatureMethod() {
    }

    public String getAlgorithmAtr() {
        return algorithmAtr;
    }

}
