package ru.enschin.adapter.model.soap.header;

/**
 * Created by Andrey on 04.03.2017.
 */
public class HeaderFactory {
    private String tokenValue = "";
    private String signatureValue = "";
    private String digestValue = "";

    public HeaderFactory setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
        return this;
    }

    public HeaderFactory setSignatureValue(String signatureValue) {
        this.signatureValue = signatureValue;
        return this;
    }

    public HeaderFactory setDigestValue(String digestValue) {
        this.digestValue = digestValue;
        return this;
    }

    public Header buildHeader() {
        Reference reference = new Reference();
        reference.setDigestValue(digestValue);

        SignedInfo signedInfo = new SignedInfo();
        signedInfo.setReference(reference);

        Signature signature = new Signature();
        signature.setSignedInfo(signedInfo);
        signature.setSignatureValue(signatureValue);

        BinarySecurityToken binarySecurityToken = new BinarySecurityToken();
        binarySecurityToken.setValue(tokenValue);

        Security security = new Security();
        security.setSignature(signature);
        security.setToken(binarySecurityToken);

        Header header = new Header(security);

        return header;
    }

    public SignedInfo buildSignedInfo() {
        Reference reference = new Reference();
        reference.setDigestValue(digestValue);

        SignedInfo signedInfo = new SignedInfo();
        signedInfo.setReference(reference);
        return signedInfo;
    }
}
