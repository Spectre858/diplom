package ru.enschin.adapter.model.soap.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Andrey on 04.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Transform {
    @XmlAttribute(name = "Algorithm")
    private String algorithmAtr = "http://www.w3.org/2001/10/xml-exc-c14n#";

    public Transform() {
    }

    public Transform(String algorithmAtr) {
        this.algorithmAtr = algorithmAtr;
    }

    public String getAlgorithmAtr() {
        return algorithmAtr;
    }

    public void setAlgorithmAtr(String algorithmAtr) {
        this.algorithmAtr = algorithmAtr;
    }
}