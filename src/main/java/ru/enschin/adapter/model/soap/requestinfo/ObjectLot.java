package ru.enschin.adapter.model.soap.requestinfo;

import ru.enschin.adapter.model.soap.requestinfo.location.Location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectLot {
    @XmlAttribute(name = "obj_kind")
    private String objKind = "002001001000";

    @XmlElement(name = "Location")
    private Location location;

    public ObjectLot() {
    }

    public String getObjKind() {
        return objKind;
    }

    public void setObjKind(String objKind) {
        this.objKind = objKind;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
