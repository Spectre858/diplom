package ru.enschin.adapter.model.soap.requestinfo.location;

import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrey on 04.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Location extends StoredEntity {
    @XmlElement(name = "Postal_Code")
    @JsonProperty
    private String postalCode;

    @XmlElement(name = "Region")
    @JsonProperty
    private String region;

    @XmlElement(name = "City")
    @JsonProperty
    private City city;

    @XmlElement(name = "Street")
    @JsonProperty
    private Street street;

    @XmlElement(name = "Level1")
    @JsonProperty
    private LocationLevel level1;

    @XmlElement(name = "Level2")
    @JsonProperty
    private LocationLevel level2;

    @XmlElement(name = "Level3")
    @JsonProperty
    private LocationLevel level3;

    @XmlElement(name = "Apartment")
    @JsonProperty
    private LocationLevel apartment;

    @XmlElement(name = "Other")
    @JsonProperty
    private String other;

    @XmlElement(name = "Note")
    @JsonProperty
    private String note;

    public Location() {
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public LocationLevel getLevel1() {
        return level1;
    }

    public void setLevel1(LocationLevel level1) {
        this.level1 = level1;
    }

    public LocationLevel getLevel2() {
        return level2;
    }

    public void setLevel2(LocationLevel level2) {
        this.level2 = level2;
    }

    public LocationLevel getLevel3() {
        return level3;
    }

    public void setLevel3(LocationLevel level3) {
        this.level3 = level3;
    }

    public LocationLevel getApartment() {
        return apartment;
    }

    public void setApartment(LocationLevel apartment) {
        this.apartment = apartment;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
