package ru.enschin.adapter.model.order;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;
import ru.enschin.adapter.model.soap.requestinfo.Agent;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;

/**
 * Created by Andrey on 04.05.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order extends StoredEntity{
    @JsonProperty
    private long declarantKind;
    @JsonProperty
    private Applicant applicant;
    @JsonProperty
    private Agent agent;
    @JsonProperty
    private Location objectLocation;
    @JsonProperty
    private String responseEmail;
    @JsonProperty
    private AppliedDocument appliedDocument;

    public Order() {
    }

    public long getDeclarantKind() {
        return declarantKind;
    }

    public void setDeclarantKind(long declarantKind) {
        this.declarantKind = declarantKind;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Location getObjectLocation() {
        return objectLocation;
    }

    public void setObjectLocation(Location objectLocation) {
        this.objectLocation = objectLocation;
    }

    public String getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(String responseEmail) {
        this.responseEmail = responseEmail;
    }

    public AppliedDocument getAppliedDocument() {
        return appliedDocument;
    }

    public void setAppliedDocument(AppliedDocument appliedDocument) {
        this.appliedDocument = appliedDocument;
    }
}
