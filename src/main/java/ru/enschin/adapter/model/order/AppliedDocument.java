package ru.enschin.adapter.model.order;

import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.dao.StoredEntity;

/**
 * Created by Andrey on 04.05.2017.
 */
public class AppliedDocument extends StoredEntity {
    @JsonProperty
    private String name;
    @JsonProperty
    private String code = "558101010000";
    @JsonProperty
    private String number;
    @JsonProperty
    private String date;
    @JsonProperty
    private StoredFile file;

    public AppliedDocument() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public StoredFile getFile() {
        return file;
    }

    public void setFile(StoredFile file) {
        this.file = file;
    }
}

