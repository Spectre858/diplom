package ru.enschin.adapter.model.order;


import org.codehaus.jackson.annotate.JsonProperty;
import ru.enschin.adapter.model.dao.StoredEntity;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;

/**
 * Created by Andrey on 04.05.2017.
 */
public class Applicant extends StoredEntity{
    @JsonProperty
    private String name;
    @JsonProperty
    private String code;
    @JsonProperty
    private String email;
    @JsonProperty
    private Location location;

    public Applicant() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
