package ru.enschin.adapter.util;

import ru.enschin.adapter.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;

/**
 * Created by Andrey on 08.05.2017.
 */
public class AuthUtil {
    private static final String USER_ATTR = "user";

    public static boolean checkAuth(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_ATTR);

        if (user == null) {
            return false;
        }
        return true;
    }

    public static User currentUser(HttpServletRequest request) {
        if (checkAuth(request)) {
            return (User) request.getSession().getAttribute(USER_ATTR);
        }
        throw new NotAuthorizedException(Response.status(401).build());
    }

    public static void setCurrentUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute(USER_ATTR, user);
    }

    public static void removeCurrentUser(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_ATTR);
    }
}
