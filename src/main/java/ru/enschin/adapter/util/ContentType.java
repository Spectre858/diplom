package ru.enschin.adapter.util;

import javax.ws.rs.core.MediaType;

/**
 * Created by Andrey on 05.06.2017.
 */
public class ContentType {
    public static final String CSS_UTF_8 = "text/css;" + MediaType.CHARSET_PARAMETER + "=UTF-8";
    public static final String JS_UTF_8 = "application/javascript;" + MediaType.CHARSET_PARAMETER + "=UTF-8";
    public static final String HTML_UTF_8 = MediaType.TEXT_HTML + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8";
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String APPLICATION_JSON_UTF_8 = MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8";

}
