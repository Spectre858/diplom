package ru.enschin.adapter.dao;

import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import org.apache.log4j.Logger;
import ru.enschin.adapter.manager.OrientDBManager;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.dao.StoredEntity;
import ru.enschin.adapter.model.dao.StoredOrder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
public class OrientDaoService implements DaoService {
    @Inject
    private OrientDBManager orient;
    @Inject
    private Logger logger;

    @Override
    public void save(StoredEntity se) {
        try (OObjectDatabaseTx db = orient.getObjectDB()) {
            db.save(se);
        }
    }

    @Override
    public List<User> selectUser(String login) {
        List<User> query = null;
        try (OObjectDatabaseTx db = orient.getObjectDB()) {
            query = db.query(new OSQLSynchQuery<User>(String.format("select from User where login = '%s'", login)));
        }
        return query;
    }

    @Override
    public void updateOrder(StoredOrder order) {
        OObjectDatabaseTx db = orient.getObjectDB();

        try {
            db.begin();

            List<StoredOrder> query = db.query(
                    new OSQLSynchQuery<StoredOrder>(String.format("select from StoredOrder where caseNumber = '%s'", order.getCaseNumber())));

            StoredOrder update = order;

            if (!query.isEmpty()) {
                update = query.get(0);
                update.setResponse(order.getResponse());
            }

            db.save(update);
            db.commit();
        } catch (Exception e) {
            db.rollback();
        } finally {
            if (!db.isClosed()) {
                db.close();
            }
        }
    }

    @Override
    public List<StoredOrder> selectAllOrdersByUser(String objectId) {
        logger.info("select all orders by user: " + objectId);
        OObjectDatabaseTx db = orient.getObjectDB();
        List<StoredOrder> query = null;
        List<StoredOrder> result = new ArrayList<>();

        try {
            query = db.query(new OSQLSynchQuery<StoredOrder>(String.format("select from StoredOrder where applicant in (select from User where objectId = '%s')", objectId)));
            logger.info(query);
            for (StoredOrder storedOrder : query) {
                StoredOrder order = db.detachAll(storedOrder, true);
                result.add(order);
            }
        } finally {
            if (!db.isClosed()) {
                db.close();
            }
        }
        return result;
    }
}

