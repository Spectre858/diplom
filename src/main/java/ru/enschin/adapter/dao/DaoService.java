package ru.enschin.adapter.dao;

import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.dao.StoredEntity;
import ru.enschin.adapter.model.dao.StoredOrder;

import java.util.List;

/**
 * Created by Andrey on 05.06.2017.
 */
public interface DaoService {
    public void save(StoredEntity se);
    public List<User> selectUser(String login);

    void updateOrder(StoredOrder order);

    List<StoredOrder> selectAllOrdersByUser(String objectId);
}
