package ru.enschin.adapter.manager;

import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Andrey on 29.04.2017.
 */
@ApplicationScoped
public class FileManager {
    @Inject
    private Logger logger;

    public static final String FILE_SEPARATOR = File.separator.equals("/") ? "/" : "\\\\";

    public File getFile(String filename) throws Exception {
        String url = new File(getResourceFolderURL().toURI()).getAbsolutePath();
        url += File.separator + filename;
        File file = new File(url);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public File getWebFile(String name) throws Exception {
        File file = new File(getResourceFolderURL().toURI());
        file = file.getParentFile().getParentFile();
        String url = file.getAbsolutePath();
        url += File.separator + name;
        file = new File(url);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public File createFile(String path) throws Exception {
        String url = new File(getResourceFolderURL().toURI()).getAbsolutePath();
        String[] part = path.split("/");

        for (int i = 0; i < part.length - 1; ++i) {
            url += File.separator + part[i];
            File file = new File(url);
            if (!file.exists()) {
                file = new File(url);
                boolean mkdir = file.mkdir();
                if (!mkdir) {
                    return null;
                }
            }
        }

        url += File.separator + part[part.length - 1];
        File resultFile = new File(url);
        if (resultFile.exists()) {
            throw new EntityExistsException("Entity with path [" + url + "] already exist");
        }
        return resultFile.createNewFile() ? resultFile : null;
    }

    public List<File> getAllFileWithFilter(final String filter) throws Exception {
        List<File> result = new ArrayList<>();
        List<File> resources = getAllFolders();

        for (File resource : resources) {
            File[] list = resource.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(filter);
                }
            });
            if (list != null) {
                Collections.addAll(result, list);
            }
        }
        return result;
    }

    public File getResource(String filename) throws Exception {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(filename);
        if (resource != null) {
            resource = new URL(resource.toString().replace(resource.getProtocol(), "file"));
            File file = new File(resource.toURI());
            if (file.exists()) {
                return file;
            }
        }
        return null;
    }

    public List<File> getAllFolders() throws Exception {
        List<File> resources = new ArrayList<>();
        resources.add(new File(getResourceFolderURL().toURI()));

        for (int i = 0; i < resources.size(); ++i) {
            File dir = resources.get(i);
            File[] tmpList = dir.listFiles();
            if (tmpList == null) {
                continue;
            }
            for (File file : tmpList) {
                if (file.isDirectory()) {
                    resources.add(resources.size(), file);
                }
            }
        }
        return resources;
    }

    private URL getResourceFolderURL() throws Exception {
        URL url = Thread.currentThread().getContextClassLoader().getResource("app.properties");
        url = new URL(url.toString().replace("/app.properties", "").replace(url.getProtocol(), "file"));
        return url;
    }

}
