package ru.enschin.adapter.manager;

import com.orientechnologies.orient.core.Orient;
import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.*;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import org.apache.log4j.Logger;
import ru.enschin.adapter.model.Error;
import ru.enschin.adapter.model.RestResponse;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.model.dao.StoredOrder;
import ru.enschin.adapter.model.order.Applicant;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.services.FileService;
import ru.enschin.adapter.services.ReflectionService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by Andrey on 04.06.2017.
 */
@ApplicationScoped
@Startup
public class OrientDBManager implements ODatabaseThreadLocalFactory{
    @Inject
    private PropertiesManager propertiesManager;
    @Inject
    private Logger logger;
    @Inject
    private FileService fileService;
    @Inject
    private ReflectionService reflectionService;

    private OPartitionedDatabasePool pool;


    @PostConstruct
    private void init() {
        logger.info("init orientdb service");

        // Config for client
        OGlobalConfiguration.CLIENT_CONNECT_POOL_WAIT_TIMEOUT.setValue(5000);
        OGlobalConfiguration.CLIENT_DB_RELEASE_WAIT_TIMEOUT.setValue(5000);

        OGlobalConfiguration.CLIENT_CHANNEL_MAX_POOL.setValue(100);

        OGlobalConfiguration.DB_POOL_MIN.setValue(10);
        OGlobalConfiguration.DB_POOL_MAX.setValue(100);
        OGlobalConfiguration.STORAGE_LOCK_TIMEOUT.setValue(5000);
        OGlobalConfiguration.DB_POOL_IDLE_TIMEOUT.setValue(5000);
        OGlobalConfiguration.SCRIPT_POOL.setValue(100);

        Orient.instance().registerThreadDatabaseFactory(this);

        pool = new OPartitionedDatabasePool("remote:localhost/adapter", propertiesManager.getProperty("orient.auth.login"),
                propertiesManager.getProperty("orient.auth.password"));

        initSchema();

        logger.info("orient started");

    }

    @PreDestroy
    private void shutdown() {
        logger.info("Start to shutdown orient");
        if (pool != null && !pool.isClosed()) {
            pool.close();
        }
        Orient.instance().shutdown();
        logger.info("shutdown");
    }

    public void initSchema() {
        OObjectDatabaseTx db = getObjectDB();
        try {
            db.setAutomaticSchemaGeneration(true);

            logger.info("registration of classes: ");

            for (Class<?> clazz : reflectionService.getStoredEntityClasses()) {
                db.getEntityManager().registerEntityClass(clazz);
                logger.info(" -> " + clazz);
            }

            db.getMetadata().getSchema().synchronizeSchema();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (!db.isClosed()) {
                db.close();
            }
        }

    }

    public OObjectDatabaseTx getObjectDB() {
        OObjectDatabaseTx db = new OObjectDatabaseTx(pool.acquire());
        db.activateOnCurrentThread();
        return db;
    }

    @Override
    public ODatabaseDocumentInternal getThreadDatabase() {
        ODatabaseDocumentTx acquire = pool.acquire();
        acquire.activateOnCurrentThread();
        return acquire;
    }
}
