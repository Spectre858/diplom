package ru.enschin.adapter.manager.producer;

import ru.enschin.adapter.manager.PropertiesManager;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

/**
 * Created by Andrey on 30.04.2017.
 */
public class PropertyAccessor {

    @Inject
    private PropertiesManager propertiesManager;

    @Produces
    @ResourceProperty
    public synchronized String getProperty(InjectionPoint injectionPoint) {
        ResourceProperty annotation = injectionPoint.getAnnotated()
                .getAnnotation(ResourceProperty.class);
        String value = annotation.value();

        return propertiesManager.getProperty(value);
    }
}
