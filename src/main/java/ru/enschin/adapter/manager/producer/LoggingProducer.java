package ru.enschin.adapter.manager.producer;

import org.apache.log4j.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * Created by Andrey on 30.04.2017.
 */
public class LoggingProducer {
    @Produces
    public synchronized Logger produce(InjectionPoint ip) {
        return Logger.getLogger(ip.getMember().getDeclaringClass().getName());
    }
}
