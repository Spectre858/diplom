package ru.enschin.adapter.manager;

import org.apache.log4j.Logger;
import ru.enschin.adapter.services.FileService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.*;

/**
 * Created by Andrey on 29.04.2017.
 */
@ApplicationScoped
public class PropertiesManager {
    @Inject
    private FileService fileService;

    @Inject
    private Logger logger;

    private Map<String, String> properties;

    public PropertiesManager() {
        properties = new HashMap<String, String>();
    }

    @PostConstruct
    public void init() {
        List<File> propertyFiles = fileService.getAllPropertyFiles();
        List<Properties> files = new ArrayList<Properties>();

        for (File file : propertyFiles) {
            Properties prop = new Properties();
            try {
                prop.load(new FileInputStream(file));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
            files.add(prop);
        }

        for (Properties prop : files) {
            for (Map.Entry entry : prop.entrySet()) {
                properties.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
        logger.info("Properties: " + properties);
    }

    public String getProperty(String key) {
        return properties.get(key);
    }


}
