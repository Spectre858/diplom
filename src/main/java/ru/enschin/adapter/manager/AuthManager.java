package ru.enschin.adapter.manager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.apache.xml.security.stax.securityToken.SecurityTokenFactory;
import ru.enschin.adapter.dao.DaoService;
import ru.enschin.adapter.model.User;
import ru.enschin.adapter.util.AuthUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.net.PasswordAuthentication;
import java.security.MessageDigest;
import java.util.List;

/**
 * Created by Andrey on 05.06.2017.
 */
@ApplicationScoped
public class AuthManager {
    @Inject
    private DaoService daoService;
    @Inject
    private Logger logger;

    public boolean login(HttpServletRequest request, User user) {
        logger.info("login: " + user);
        List<User> list = daoService.selectUser(user.getLogin());
        if (list.isEmpty()) {
            return false;
        }

        User exists = list.get(0);
        MessageDigest sha512 = DigestUtils.getSha512Digest();
        sha512.update(user.getPassword().getBytes());
        String hashPass = Base64.encodeBase64String(sha512.digest());
        logger.info("hash: " + hashPass);

        if (exists.getPassword().equals(hashPass)) {
            AuthUtil.setCurrentUser(request, exists);
            return true;
        }
        return false;
    }


    public boolean create(HttpServletRequest request, User user) {
        List<User> list = daoService.selectUser(user.getLogin());
        if (!list.isEmpty()) {
            return false;
        }

        MessageDigest sha512 = DigestUtils.getSha512Digest();
        sha512.update(user.getPassword().getBytes());
        String hashPass = Base64.encodeBase64String(sha512.digest());

        user.setPassword(hashPass);

        daoService.save(user);
        AuthUtil.setCurrentUser(request, user);
        return true;
    }
}
