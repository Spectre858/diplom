package ru.enschin.adapter.model.soap.requestinfo;

import org.junit.Before;
import org.junit.Test;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.order.Applicant;
import ru.enschin.adapter.model.order.AppliedDocument;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.requestinfo.document.Document;
import ru.enschin.adapter.model.soap.requestinfo.location.City;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;
import ru.enschin.adapter.model.soap.requestinfo.location.LocationLevel;
import ru.enschin.adapter.model.soap.requestinfo.location.Street;
import ru.enschin.adapter.services.XmlService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;

/**
 * Created by Andrey on 04.05.2017.
 */
public class RequestInfoFactoryTest {
    private RequestInfoFactory requestInfoFactory;
    private XmlService xmlService;

    @Before
    public void init() {
        requestInfoFactory = spy(RequestInfoFactory.class);
        xmlService = spy(XmlService.class);
    }

    @Test
    public void build() throws Exception {
        City city = new City();
        city.setName("Новосибирск");
        city.setType("г");

        Street street = new Street();
        street.setName("Беловежская");
        street.setType("ул");

        LocationLevel level = new LocationLevel();
        level.setType("д");
        level.setValue("4/1");

        LocationLevel kv = new LocationLevel();
        kv.setType("кв");
        kv.setValue("250");

        Location location = new Location();
        location.setRegion("54");
        location.setCity(city);
        location.setStreet(street);
        location.setLevel1(level);
        location.setApartment(kv);

        Applicant applicant = new Applicant();
        applicant.setName("Пенсионный фонд россии");
        applicant.setCode("1234");
        applicant.setEmail("test@test.com");
        applicant.setLocation(location);

        Fio fio = new Fio();
        fio.setFirst("Andrey");
        fio.setSurname("Enshin");

        Document document = new Document();
        document.setSeries("1234");
        document.setNumber("123456");
        document.setDate("15.04.2013");
        document.setIssueOrgan("УФМС");

        Agent agent = new Agent();
        agent.setFio(fio);
        agent.setDocument(document);
        agent.setLocation(location);
        agent.setEmail("andrey@gmail.com");
        agent.setSnils("1235-1235");
        agent.setPhone("999-132-1123");

        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName("Заявление");
        appliedDocument.setFile(new StoredFile("1", "order.pdf", null));

        Order order = new Order();
        order.setApplicant(applicant);
        order.setAgent(agent);
        order.setObjectLocation(location);
        order.setAppliedDocument(appliedDocument);
        order.setResponseEmail("spectre@mail.ru");

        requestInfoFactory.setOrder(order);

        RequestGKN requestGKN = requestInfoFactory.build();

        String xml = xmlService.objectToXml(requestGKN, RequestGKN.class);

        System.out.println(xml);

        assertNotNull(xml);


    }

}