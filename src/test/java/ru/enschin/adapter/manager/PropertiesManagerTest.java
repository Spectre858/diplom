package ru.enschin.adapter.manager;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.enschin.adapter.services.FileService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.powermock.reflect.Whitebox.setInternalState;

/**
 * Created by Andrey on 29.04.2017.
 */
public class PropertiesManagerTest {
    private Logger logger;
    private FileService fileService;
    private FileManager fileManager;
    private PropertiesManager propertiesManager;
    @Before
    public void init() {
        logger = mock(Logger.class);
        fileService = spy(FileService.class);
        fileManager = spy(FileManager.class);

        setInternalState(fileManager, "logger", logger);

        setInternalState(fileService, "logger", logger);
        setInternalState(fileService, "fileManager", fileManager);

        propertiesManager = spy(PropertiesManager.class);

        setInternalState(propertiesManager, "logger", logger);
        setInternalState(propertiesManager, "fileService", fileService);

        propertiesManager.init();
    }
    @Test
    public void testGetProperty() throws Exception {
        String p = propertiesManager.getProperty("rest.folder");
        Assert.assertEquals("ru.enschin.adapter.rest", p);
    }

}