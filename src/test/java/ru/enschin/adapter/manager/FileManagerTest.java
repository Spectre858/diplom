package ru.enschin.adapter.manager;

import org.apache.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;

import java.io.File;
import java.util.List;

/**
 * Created by Andrey on 29.04.2017.
 */
public class FileManagerTest {

    private Logger logger;
    private FileManager fileManager;

    @Before
    public void init() {
        logger = Mockito.mock(Logger.class);
        fileManager = Mockito.spy(FileManager.class);
        Whitebox.setInternalState(fileManager, "logger", logger);

    }


    @Test
    public void getAllFolders() throws Exception {
        List<File> allFolders = fileManager.getAllFolders();
        System.out.println(allFolders);

    }

    @Test
    public void getAllFilesWithFilterTest() throws Exception {
        List<File> allFileWithFilter = fileManager.getAllFileWithFilter(".properties");
        System.out.println(allFileWithFilter);
    }

    @Test
    public void getResourceTest() throws Exception {
        File file = fileManager.getResource("sign/rnd-4-83ab-849f-bf72-9154-3174-cbe6-8036");
        System.out.println(file);
    }

    @Test
    public void createFileTest() throws Exception {
        File result = fileManager.createFile("testFolder/file.txt");
        Assert.assertNotNull(result);
    }


    @Test
    public void getFileTest() throws Exception {
        File file = fileManager.getFile("testFolder/folder");
        System.out.println(file);

    }
}