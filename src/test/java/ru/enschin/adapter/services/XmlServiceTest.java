package ru.enschin.adapter.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.enschin.adapter.model.soap.Envelope;

import static org.mockito.Mockito.spy;

/**
 * Created by Andrey on 01.05.2017.
 */
public class XmlServiceTest {
    private XmlService xmlService;

    @Before
    public void init() throws Exception {
        xmlService = spy(XmlService.class);
    }

    @Test
    public void testObjectToXml() throws Exception {
        Envelope envelope = new Envelope();
        Assert.assertNotNull(xmlService.objectToXml(envelope, Envelope.class));
    }

}