package ru.enschin.adapter.services;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import ru.enschin.adapter.manager.FileManager;
import ru.enschin.adapter.manager.PropertiesManager;
import ru.enschin.adapter.model.RestResponse;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.order.Applicant;
import ru.enschin.adapter.model.order.AppliedDocument;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.Envelope;
import ru.enschin.adapter.model.soap.body.*;
import ru.enschin.adapter.model.soap.header.HeaderFactory;
import ru.enschin.adapter.model.soap.requestinfo.Agent;
import ru.enschin.adapter.model.soap.requestinfo.Fio;
import ru.enschin.adapter.model.soap.requestinfo.RequestGKN;
import ru.enschin.adapter.model.soap.requestinfo.RequestInfoFactory;
import ru.enschin.adapter.model.soap.requestinfo.document.Document;
import ru.enschin.adapter.model.soap.requestinfo.location.City;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;
import ru.enschin.adapter.model.soap.requestinfo.location.LocationLevel;
import ru.enschin.adapter.model.soap.requestinfo.location.Street;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Created by Andrey on 06.05.2017.
 */
public class SOAPServiceTest {
    private XmlService xmlService;
    private CompressionService compressionService;
    private CryptoService cryptoService;
    private FileManager fileManager;
    private FileService fileService;
    private Logger logger;
    private SOAPService soapService;
    private PropertiesManager propertiesManager;

    private HeaderFactory headerFactory;
    private BodyFactory bodyFactory;

    @Before
    public void setUp() throws Exception {
        logger = mock(Logger.class);

        compressionService = spy(CompressionService.class);
        Whitebox.setInternalState(compressionService, "logger", logger);

        fileManager = spy(FileManager.class);
        Whitebox.setInternalState(fileManager, "logger", logger);

        fileService = spy(FileService.class);
        Whitebox.setInternalState(fileService, "fileManager", fileManager);
        Whitebox.setInternalState(fileService, "logger", logger);

        propertiesManager = spy(PropertiesManager.class);
        Whitebox.setInternalState(propertiesManager, "fileService", fileService);
        Whitebox.setInternalState(propertiesManager, "logger", logger);
        propertiesManager.init();

        cryptoService = spy(CryptoService.class);
        Whitebox.setInternalState(cryptoService, "fileService", fileService);
        Whitebox.setInternalState(cryptoService, "logger", logger);
        Whitebox.setInternalState(cryptoService, "signAlgorithm", propertiesManager.getProperty("crypto.sign.algorithm"));
        Whitebox.setInternalState(cryptoService, "signProvider", propertiesManager.getProperty("crypto.sign.provider"));
        Whitebox.setInternalState(cryptoService, "keyContainer", propertiesManager.getProperty("crypto.key.container"));
        Whitebox.setInternalState(cryptoService, "signVersion", propertiesManager.getProperty("crypto.sign.version"));

        cryptoService.initBeginParams();

        xmlService = spy(XmlService.class);

        soapService = spy(SOAPService.class);
        Whitebox.setInternalState(soapService, "xmlService", xmlService);
        Whitebox.setInternalState(soapService, "cryptoService", cryptoService);
        Whitebox.setInternalState(soapService, "destinationURL", propertiesManager.getProperty("soap.destination.url"));
        soapService.init();

        City city = new City();
        city.setName("Новосибирск");
        city.setType("г");

        Street street = new Street();
        street.setName("Беловежская");
        street.setType("ул");

        LocationLevel level = new LocationLevel();
        level.setType("д");
        level.setValue("4/1");

        LocationLevel kv = new LocationLevel();
        kv.setType("кв");
        kv.setValue("250");

        Location location = new Location();
        location.setRegion("54");
        location.setCity(city);
        location.setStreet(street);
        location.setLevel1(level);
        location.setApartment(kv);

        Applicant applicant = new Applicant();
        applicant.setName("Пенсионный фонд россии");
        applicant.setCode("1234");
        applicant.setEmail("test@test.com");
        applicant.setLocation(location);

        Fio fio = new Fio();
        fio.setFirst("Andrey");
        fio.setSurname("Enshin");

        Document document = new Document();
        document.setSeries("1234");
        document.setNumber("123456");
        document.setDate("15.04.2013");
        document.setIssueOrgan("УФМС");

        Agent agent = new Agent();
        agent.setFio(fio);
        agent.setDocument(document);
        agent.setLocation(location);
        agent.setEmail("andrey@gmail.com");
        agent.setSnils("1235-1235");
        agent.setPhone("999-132-1123");

        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName("Заявление");
        appliedDocument.setFile(new StoredFile("1", "order.pdf", null));

        Order order = new Order();
        order.setApplicant(applicant);
        order.setAgent(agent);
        order.setObjectLocation(location);
        order.setAppliedDocument(appliedDocument);
        order.setResponseEmail("spectre@mail.ru");

        String uuid = UUID.randomUUID().toString();
//        String uuid = "c3b4b9e6-6a0c-4b66-9af4-d8f3860aa5a9";
        RequestInfoFactory requestInfoFactory = new RequestInfoFactory();
        requestInfoFactory.setOrder(order);
        requestInfoFactory.setUuid(uuid);

        RequestGKN requestGKN = requestInfoFactory.build();
        String xml = xmlService.objectToXml(requestGKN, RequestGKN.class);

        xml = cryptoService.toCanonicalize(xml.getBytes("UTF-8"));

//        Files.write(
//                Paths.get("gkn.xml"), xml.getBytes(),
//                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);


        List<String> files = new ArrayList<>();
        files.add("req_" + uuid + ".xml");
        files.add("req_" + uuid + ".xml.sig");

//        byte[] ord = Files.readAllBytes(Paths.get("order.pdf"));
        byte[] req = Files.readAllBytes(Paths.get("req.xml"));
        String sign = cryptoService.getSignature(req);

        List<String> data = new ArrayList<>();

        data.add(new String(req,"ISO-8859-1"));
        data.add(sign);

        byte[] zip = compressionService.toZip(files, data, "ISO-8859-1");
        Files.write(
                Paths.get("test.zip"), zip,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        xml = Base64.encodeBase64String(zip);

        headerFactory = new HeaderFactory();

        AppDocument appDocument = new AppDocument();
        appDocument.setBinaryData(xml);
        appDocument.setRequestCode("req_" + uuid + ".xml");
        AppDataCreateRequest appDataCreateRequest = new AppDataCreateRequest(new CreateRequestBeen());
        AppDataGetStatus appDataGetStatus = new AppDataGetStatus("59-280923");
        bodyFactory = new BodyFactory()
                .setDate("2015-02-09T15:36:54.928+07:00")
                .setAppDocument(appDocument)
                .setAppDataCreateRequest(appDataCreateRequest)
                .setAppDataGetStatus(appDataGetStatus)
                .setOriginator(new User("FMS001001", "ФМС"))
                .setRecipient(new User("RRTR01001", "Росреестр"))
                .setSender(new User("FMS001001", "ФМС"));
    }

    @Test
    public void buildCreateOrderSoapRequestTest() throws Exception {
        String soap = soapService.buildCreateOrderSoapRequest(headerFactory, bodyFactory);

        Files.write(Paths.get("createOrder.xml"), soap.getBytes("UTF-8"),
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        Assert.assertNotNull(soap);
    }

    @Test
    public void buildGetStatusSoapRequestTest() throws Exception {
        String soap = soapService.buildGetStatusSoapRequest(headerFactory, bodyFactory);

        Files.write(Paths.get("getStatus.xml"), soap.getBytes(),
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        Assert.assertNotNull(soap);
    }

    @Test
    public void sendSoapMessageTest() throws Exception {
        String soap = soapService.buildCreateOrderSoapRequest(headerFactory, bodyFactory);
        Files.write(
                Paths.get("soap.xml"), soap.getBytes(),
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        RestResponse response = soapService.sendMessage(soap);
        System.out.println(response.getCode() + "\n" + response.getMessage());

        if (response.getError() != null) {
            System.out.println(response.getError().getCode() + "\n" + response.getError().getMessage());
        }

        assertNotNull(response.getCode());
    }

}