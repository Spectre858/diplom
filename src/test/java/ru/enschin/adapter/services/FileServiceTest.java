package ru.enschin.adapter.services;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import ru.enschin.adapter.manager.FileManager;

import javax.crypto.KeyGenerator;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Created by Andrey on 08.05.2017.
 */
public class FileServiceTest {
    private FileService fileService;
    private FileManager fileManager;
    private Logger logger;

    @Before
    public void init() {
        logger = mock(Logger.class);
        fileManager = spy(FileManager.class);
        Whitebox.setInternalState(fileManager, "logger", logger);

        fileService = spy(FileService.class);
        Whitebox.setInternalState(fileService, "logger", logger);
        Whitebox.setInternalState(fileService, "fileManager", fileManager);
    }
    @Test
    public void readFile() throws Exception {
        String s = fileService.readFile("order.pdf");
        Files.write(
                Paths.get("order_test.pdf"), s.getBytes(),
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    public void test() {
        System.out.println(UUID.randomUUID().toString());
    }


}