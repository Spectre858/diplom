package ru.enschin.adapter.services;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.xml.sax.SAXException;
import ru.enschin.adapter.manager.FileManager;
import ru.enschin.adapter.manager.PropertiesManager;
import ru.enschin.adapter.model.StoredFile;
import ru.enschin.adapter.model.order.Applicant;
import ru.enschin.adapter.model.order.AppliedDocument;
import ru.enschin.adapter.model.order.Order;
import ru.enschin.adapter.model.soap.requestinfo.Agent;
import ru.enschin.adapter.model.soap.requestinfo.Fio;
import ru.enschin.adapter.model.soap.requestinfo.RequestGKN;
import ru.enschin.adapter.model.soap.requestinfo.RequestInfoFactory;
import ru.enschin.adapter.model.soap.requestinfo.document.Document;
import ru.enschin.adapter.model.soap.requestinfo.location.City;
import ru.enschin.adapter.model.soap.requestinfo.location.Location;
import ru.enschin.adapter.model.soap.requestinfo.location.LocationLevel;
import ru.enschin.adapter.model.soap.requestinfo.location.Street;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.DataFormatException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Created by Andrey on 06.05.2017.
 */
public class CompressionServiceTest {
    private XmlService xmlService;
    private CompressionService compressionService;
    private CryptoService cryptoService;
    private FileManager fileManager;
    private Logger logger;
    private PropertiesManager propertiesManager;
    private FileService fileService;

    @Before
    public void setUp() throws Exception {
        logger = mock(Logger.class);
        compressionService = spy(CompressionService.class);
        Whitebox.setInternalState(compressionService, "logger", logger);

        xmlService = spy(XmlService.class);

        fileManager = spy(FileManager.class);
        Whitebox.setInternalState(fileManager, "logger", logger);

        fileService = spy(FileService.class);
        Whitebox.setInternalState(fileService, "logger", logger);
        Whitebox.setInternalState(fileService, "fileManager", fileManager);

        propertiesManager = spy(PropertiesManager.class);
        Whitebox.setInternalState(propertiesManager, "fileService", fileService);
        Whitebox.setInternalState(propertiesManager, "logger", logger);
        propertiesManager.init();

        cryptoService = spy(CryptoService.class);
        Whitebox.setInternalState(cryptoService, "fileService", fileService);
        Whitebox.setInternalState(cryptoService, "logger", logger);
        Whitebox.setInternalState(cryptoService, "signAlgorithm", propertiesManager.getProperty("crypto.sign.algorithm"));
        Whitebox.setInternalState(cryptoService, "signProvider", propertiesManager.getProperty("crypto.sign.provider"));
        Whitebox.setInternalState(cryptoService, "keyContainer", propertiesManager.getProperty("crypto.key.container"));
        Whitebox.setInternalState(cryptoService, "signVersion", propertiesManager.getProperty("crypto.sign.version"));
        cryptoService.initBeginParams();
    }


    @Test
    public void fullTest() throws JAXBException, IOException, InvalidCanonicalizerException, ParserConfigurationException, SAXException, CanonicalizationException, DataFormatException {
        City city = new City();
        city.setName("Новосибирск");
        city.setType("г");

        Street street = new Street();
        street.setName("Беловежская");
        street.setType("ул");

        LocationLevel level = new LocationLevel();
        level.setType("д");
        level.setValue("4/1");

        LocationLevel kv = new LocationLevel();
        kv.setType("кв");
        kv.setValue("250");

        Location location = new Location();
        location.setRegion("54");
        location.setCity(city);
        location.setStreet(street);
        location.setLevel1(level);
        location.setApartment(kv);

        Applicant applicant = new Applicant();
        applicant.setName("Пенсионный фонд россии");
        applicant.setCode("1234");
        applicant.setEmail("test@test.com");
        applicant.setLocation(location);

        Fio fio = new Fio();
        fio.setFirst("Andrey");
        fio.setSurname("Enshin");

        Document document = new Document();
        document.setSeries("1234");
        document.setNumber("123456");
        document.setDate("15.04.2013");
        document.setIssueOrgan("УФМС");

        Agent agent = new Agent();
        agent.setFio(fio);
        agent.setDocument(document);
        agent.setLocation(location);
        agent.setEmail("andrey@gmail.com");
        agent.setSnils("1235-1235");
        agent.setPhone("999-132-1123");

        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName("Заявление");
        appliedDocument.setFile(new StoredFile("1", "order.pdf", null));

        Order order = new Order();
        order.setApplicant(applicant);
        order.setAgent(agent);
        order.setObjectLocation(location);
        order.setAppliedDocument(appliedDocument);
        order.setResponseEmail("spectre@mail.ru");

        RequestInfoFactory requestInfoFactory = new RequestInfoFactory();
        requestInfoFactory.setOrder(order);

        RequestGKN requestGKN = requestInfoFactory.build();
        String xml = xmlService.objectToXml(requestGKN, RequestGKN.class);

        xml = cryptoService.toCanonicalize(xml.getBytes("UTF-8"));
        List<String> names = new ArrayList<>();
        names.add("req.xml");
        List<String> data = new ArrayList<>();
        data.add(xml);
        byte[] zip = compressionService.toZip(names, data, "UTF-8");
        List<String> resultNames = new ArrayList<>();
        List<String> resultData = new ArrayList<>();

        String xmlBase64 = Base64.encodeBase64String(zip);
        compressionService.unZip(resultNames, resultData, new String(Base64.decodeBase64(xmlBase64)), "UTF-8");

        assertArrayEquals(names.toArray(), resultNames.toArray());
        assertArrayEquals(data.toArray(), resultData.toArray());
    }

    @Test
    public void customTest() throws DataFormatException, IOException, JAXBException {
        City city = new City();
        city.setName("Новосибирск");
        city.setType("г");

        Street street = new Street();
        street.setName("Беловежская");
        street.setType("ул");

        LocationLevel level = new LocationLevel();
        level.setType("д");
        level.setValue("4/1");

        LocationLevel kv = new LocationLevel();
        kv.setType("кв");
        kv.setValue("250");

        Location location = new Location();
        location.setRegion("54");
        location.setCity(city);
        location.setStreet(street);
        location.setLevel1(level);
        location.setApartment(kv);

        Applicant applicant = new Applicant();
        applicant.setName("Пенсионный фонд россии");
        applicant.setCode("1234");
        applicant.setEmail("test@test.com");
        applicant.setLocation(location);

        Fio fio = new Fio();
        fio.setFirst("Andrey");
        fio.setSurname("Enshin");

        Document document = new Document();
        document.setSeries("1234");
        document.setNumber("123456");
        document.setDate("15.04.2013");
        document.setIssueOrgan("УФМС");

        Agent agent = new Agent();
        agent.setFio(fio);
        agent.setDocument(document);
        agent.setLocation(location);
        agent.setEmail("andrey@gmail.com");
        agent.setSnils("1235-1235");
        agent.setPhone("999-132-1123");

        AppliedDocument appliedDocument = new AppliedDocument();
        appliedDocument.setName("Заявление");
        appliedDocument.setFile(new StoredFile("1", "order.pdf", null));

        Order order = new Order();
        order.setApplicant(applicant);
        order.setAgent(agent);
        order.setObjectLocation(location);
        order.setAppliedDocument(appliedDocument);
        order.setResponseEmail("spectre@mail.ru");

        RequestInfoFactory requestInfoFactory = new RequestInfoFactory();
        requestInfoFactory.setOrder(order);

        RequestGKN requestGKN = requestInfoFactory.build();
        String xml = xmlService.objectToXml(requestGKN, RequestGKN.class);



        List<String> files = new ArrayList<>();
        files.add("_request.xml");
        files.add("order.pdf");


        List<String> data = new ArrayList<>();
        data.add(xml);
        data.add(fileService.readFile("order.pdf"));
        byte[] zip = compressionService.toZip(files, data, "UTF-8");

        Files.write(
                Paths.get("test1.zip"), zip,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    public void someTest() throws IOException {
        String string = "UEsDBBQACAgIACNvt0oAAAAAAAAAAAAAAAAHAAAAcmVxLnhtbO1XzW7TQBC+8xSW74nXcRwaaWM1atoqamiAUA5coo29TV0cO/inUm+t4MYBceICQggeAEqLSqv2GTZvxOzacZPYThH0wIEoinbntzP7fTtb/Ji+iGgQbm5tG/ck+GDa8sxoRN1Q2txptxqyqQ2qgzqtlWoEmaXqoFYr1clutWSt7Gr1GiKkaumy9JT6ge25DVktI002sJKGScImeeKdkLSo6RCfQCJruuo/t12rIWv6fYTgyz/yjYfw2vQOqO8S16TzCqHcJiNqsI/sjF1Njtk5u2ZXsHrNfkqTV2JzKk2O2PXkWGjPsSIcsnFukvTXPIsaakWrYmVRmvVbL42I7RghFLrKf8qmN8JKIs2adzyThNC1rCpp2ZArdUidLPPt1uzwUOKVNGT2Aco84RVCfd/Y+eQIVhey9ORwzLXf+clw84JIvdCnNJzGegt9vBTxztgPHod9nbyZxpq8ZJc8WuxSEK9DD6ijTrOfAkyIE8GyqqjcN1YX+DbHxA8FDhP3C3aSBqjoiAdIbXKaqxR3FzeHuT5Ct9Hu5mviDkWAAMDMuhvs2S6Un+yLPTZsH2DfdC2fHmIl3uVnVgpT43ky5ZpwSPZTO4RWEFLFF8GRz+mWVEd9mwYJ3JNNsfV2NBpQX1jrNeBSvC22b5EQqKSXUbVcQaqGFSEotm8HQUS7/pC4BvvMvrD37BNWZoQFXVxe5y2kEya/STxhe2fkE9HumIBxwX9OQuH/l0QUMZaQUeiTK5IIlqwO+Wb53Sm8Hu55LjXq9XpJ1SolFYCIlVhWcL1ttzs9jle9xH+gd0KQb0z4HSEGkqHpNYS0eB5hZUaRc+vkXC2zk2Nm/inpAJwR8jFp+9QCapCFKFu9/rOd/lbO34u7g31qhh0vlLzBfjJEEaqkN4D8nwtJwf82F7CSnuQihLKnzw8kDyzwsHJswNtCb/F6n1OpaQHFgsAIxpDHp6uCan4ETJtTz8F0MRy0YezY1EpHysKUyOhzKp0fSbq+oiJVYPX2cZU8895xBAAeLvlzD1B2Vvica4+AswWTLFYmKPN8i/rlsbXLz1Eo8s6oKBx+FAGZi19XXd8e2i5xpKkdvJbldNPv7QF2uQhyT03z0udn4bgranpWN3vADyLXNu0xcWDgH9hwRWElI4rf8Er6iE+X/P+GX1BLBwgfiOy5aQMAAEQMAABQSwECFAAUAAgICAAjb7dKH4jsuWkDAABEDAAABwAAAAAAAAAAAAAAAAAAAAAAcmVxLnhtbFBLBQYAAAAAAQABADUAAACeAwAAAAA=";
        byte[] bytes = Base64.decodeBase64(string);
        Files.write(
                Paths.get("test2.zip"), bytes,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    public void mainTest() throws IOException {
//        byte[] bytes = Files.readAllBytes(Paths.get("decode.b64"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        System.out.println(sdf.format(new Date()));

//        List<String> files = new ArrayList<>();
//        files.add("order_n.pdf");
//
//        List<String> data = new ArrayList<>();
//        data.add(new String(bytes, "ISO-8859-1"));
//        byte[] zip = compressionService.toZip(files, data, "ISO-8859-1");
//        Files.write(Paths.get("test.zip"), Base64.decodeBase64(bytes),
//                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
    }
}